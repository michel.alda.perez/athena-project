import { Component, OnInit, DoCheck, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StudyCard } from '../../../interfaces/StudyCard.interface';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StudySet } from 'src/app/interfaces/StudySet.interface';
import { StudyCardsService } from '../../../services/study-cards.service';

interface DialogData {
  title: string;
  content: string;
  studySet: StudySet;
  card?: StudyCard;
  createCard?: boolean;
}

@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.scss'],
})
export class EditCardComponent implements OnInit, DoCheck {
  card: StudyCard;
  cardForm: FormGroup;
  validForm = false;
  currentStudySet: StudySet;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private formBuilder: FormBuilder,
    private router: ActivatedRoute,
    private studyCardService: StudyCardsService
  ) {}

  ngDoCheck(): void {
    this.validForm = this.cardForm.valid ? true : false;
  }

  ngOnInit(): void {
    if (this.data.studySet) {
      this.currentStudySet = this.data.studySet;
    }
    this.cardForm = this.formBuilder.group({
      study_set: [''],
      term: ['', [Validators.required, Validators.maxLength(50)]],
      definition: ['', [Validators.required, Validators.maxLength(100)]],
      card_id: [''],
    });
    if (this.currentStudySet) {
      this.cardForm.patchValue({
        study_set: this.currentStudySet.id,
      });
    } else {
      this.card = this.data.card;
      this.cardForm.patchValue({
        term: this.card.term,
        definition: this.card.definition,
        card_id: this.data.card.id,
      });
    }
  }

  handleForm() {
    if (this.cardForm.invalid) {
      Object.values(this.cardForm.controls).forEach((control) => {
        control.markAsDirty();
        control.markAsUntouched();
      });
    }
  }
}
