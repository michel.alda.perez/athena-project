import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StudySet } from 'src/app/interfaces/StudySet.interface';
import { StudySetsService } from 'src/app/services/study-sets.service';

export interface DialogData {
  title: string;
  content: string;
  studySet?: StudySet;
  actualUserId?: string;
}

@Component({
  selector: 'app-preview-cards',
  templateUrl: './preview-cards.component.html',
  styleUrls: ['./preview-cards.component.scss'],
})
export class PreviewCardsComponent implements OnInit {
  studySet: StudySet;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private studySetsService: StudySetsService
  ) {}

  ngOnInit(): void {
    this.studySetsService
      .getOneStudySet(this.data.studySet.id)
      .subscribe((resp) => (this.studySet = resp));
  }
}
