import { Component, OnInit, DoCheck, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FoldersService } from 'src/app/services/folders.service';
import { Router } from '@angular/router';
import { Folder } from 'src/app/interfaces/Folder.interface';

export interface DialogData {
  title: string;
  content: string;
  createFolder?: boolean;
  folder?: Folder;
  isEditing?: boolean;
}
@Component({
  selector: 'app-edit-folder',
  templateUrl: './edit-folder.component.html',
  styleUrls: ['./edit-folder.component.scss'],
})
export class EditFolderComponent implements OnInit, DoCheck {
  editForm: FormGroup;
  validForm = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private formBuilder: FormBuilder,
    private router: Router,
    private foldersService: FoldersService
  ) {}

  ngDoCheck(): void {
    this.validForm = this.editForm.valid ? true : false;
  }

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.maxLength(30)]],
      description: ['', [Validators.required, Validators.maxLength(100)]],
    });
    if (this.data.folder) {
      this.editForm.patchValue({
        title: this.data.folder.title,
        description: this.data.folder.description,
      });
    }
  }

  handleForm() {
    if (this.editForm.invalid) {
      Object.values(this.editForm.controls).forEach((control) => {
        control.markAsDirty();
        control.markAsUntouched();
      });
    } else {
      if (!this.data.isEditing) {
        this.foldersService
          .createFolder(this.editForm.value)
          .subscribe((res) => {
            this.router.navigateByUrl(`/home/folders/${res.id}`);
          });
      }
    }
  }
}
