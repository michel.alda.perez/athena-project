import {
  Component,
  OnInit,
  Inject,
  DoCheck,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { StudySet } from 'src/app/interfaces/StudySet.interface';
import { StudySetsService } from 'src/app/services/study-sets.service';
import { Router } from '@angular/router';

export interface DialogData {
  title: string;
  content: string;
  studySet?: StudySet;
  editing?: boolean;
}

@Component({
  selector: 'app-edit-study-set',
  templateUrl: './edit-study-set.component.html',
  styleUrls: ['./edit-study-set.component.scss'],
})
export class EditStudySetComponent implements OnInit, DoCheck {
  editForm: FormGroup;
  validForm = false;
  @Output() formSubmited = new EventEmitter<boolean>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private formBuilder: FormBuilder,
    private studySetsService: StudySetsService,
    private router: Router,
    public dialogRef: MatDialogRef<EditStudySetComponent>
  ) {}

  ngDoCheck(): void {
    this.editForm.valid ? (this.validForm = true) : (this.validForm = false);
  }

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.maxLength(100)]],
      description: ['', [Validators.required, Validators.maxLength(350)]],
      visibility: ['', Validators.required],
      study_set: [''],
    });
    if (this.data.studySet) {
      this.editForm.patchValue({
        study_set: this.data.studySet.id,
        title: this.data.studySet.title,
        description: this.data.studySet.description,
        visibility: this.data.studySet.visibility,
      });
    }
  }

  handleForm() {
    if (this.editForm.invalid) {
      Object.values(this.editForm.controls).forEach((control) => {
        control.markAsDirty();
        control.markAsUntouched();
      });
    } else {
      if (!this.data.editing) {
        this.studySetsService
          .createStudySet(this.editForm.value)
          .subscribe((resp) => {
            this.router.navigateByUrl(`/home/study-sets/${resp.id}`);
          });
      }
    }
  }
}
