import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit {
  @Input() inputFormControl: FormControl;
  @Input() label: string;
  @Input() placeholder: string;
  @Input() hint: string;
  @Input() iconName: string;
  @Input() type: string;
  @Input() isPassword: boolean;  
  hide = false;

  constructor() {
    this.isPassword = false;
  }

  ngOnInit(): void {
    this.hide = this.isPassword;
  }

  getErrorMessage() {
    if (this.inputFormControl.hasError('required')) {
      return 'You must enter a value';
    }
    if (this.inputFormControl.hasError('email')) {
      return 'Not a valid email';
    }
    if (this.inputFormControl.hasError('minlength')) {
      return `Please enter at least ${this.inputFormControl.errors.minlength.requiredLength} letters`;
    }
    if (this.inputFormControl.hasError('maxlength')) {
      return `Please enter max ${this.inputFormControl.errors.maxlength.requiredLength} letters`;
    }
    if (
      this.inputFormControl.hasError('pattern') &&
      this.label === 'Username'
    ) {
      return `Please use only letters and numbers`;
    }
    if (this.inputFormControl.hasError('emailNotAvailable')) {
      return `This email address is already in use`;
    }
    if (this.inputFormControl.hasError('usernameNotAvailable')) {
      return `This username is already in use`;
    }
    if (this.inputFormControl.hasError('invalidCredentials')) {
      return `Invalid Credentials`;
    }
  }
}
