import { Component, OnInit, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

import { EditStudySetComponent } from '../../edit-dialogs/edit-study-set/edit-study-set.component';
import { DialogComponent } from '../../dialog/dialog.component';
import { PreviewCardsComponent } from '../../edit-dialogs/preview-cards/preview-cards.component';
import { EditFolderComponent } from '../../edit-dialogs/edit-folder/edit-folder.component';

import {
  StudySet,
  studySetsDataTest,
} from 'src/app/interfaces/StudySet.interface';

import { AuthService } from 'src/app/services/auth.service';
import { StudySetsService } from 'src/app/services/study-sets.service';

@Component({
  selector: 'app-simple-view-card',
  templateUrl: './simple-view-card.component.html',
  styleUrls: ['./simple-view-card.component.scss'],
})
export class SimpleViewCardComponent implements OnInit {
  @Input() avatarUrl: string;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() content: string;
  @Input() isForFolder: boolean;
  @Input() isForStudySet: boolean;
  @Input() isForSearch: boolean;
  @Input() studySetId: string;
  @Input() studySet: StudySet;
  @Input() actualUserId: string;

  constructor(
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private authService: AuthService,
    private studySetsService: StudySetsService
  ) {}

  ngOnInit(): void {
    this.authService
      .getUserPhoto(this.studySet.original_user_id)
      .subscribe((res) => (this.avatarUrl = res.profile_photo));
  }

  openEditStudySetDialog() {
    let dialogRef = this.dialog.open(EditStudySetComponent, {
      data: {
        title: `${this.studySet.title} Edit`,
        content: 'You can edit the information of your study set',
        studySet: this.studySet,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'true') this.editStudySet();
    });
  }

  editStudySet() {
    this._snackBar.open(`'${this.title}'`, 'Edited Successfully', {
      duration: 2000,
    });
  }

  openDeleteDialog() {
    this.studySet = studySetsDataTest.find(
      (studySet) => this.studySetId === studySet.id
    );
    let dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: `Delete '${this.title}'`,
        content: `Do you want to delete '${this.title}' and all his content, this proccess can't be undone`,
        study_set: this.studySet,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'true') {
        this.deleteStudySet(this.studySet.id);
      }
    });
  }

  deleteStudySet(studySetId: string) {
    this._snackBar.open(`'${this.studySet.title}'`, 'Deleted Successfully', {
      duration: 2000,
    });
  }

  openFolderEditDialog() {
    let data = {
      title: `Edit Folder '${this.title}'`,
      content: `You can edit the information of your folder '${this.title}'`,
    };
    let dialogRef = this.dialog.open(EditFolderComponent, { data: data });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'true') {
        this.editStudySet();
      }
    });
  }

  editFolder() {
    this._snackBar.open(`'${this.title}'`, 'Edited Successfully', {
      duration: 2000,
    });
  }

  openSaveDialog() {
    let data = {
      title: `'${this.studySet.title}'`,
      content: `'${this.studySet.description}'`,
      study_set: this.studySet,
    };
    let dialogRef = this.dialog.open(DialogComponent, { data });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'true') {
        this.saveStudySet();
      }
    });
  }

  saveStudySet() {
    this._snackBar.open(`'${this.studySet.title}'`, 'Saved Successfully', {
      duration: 2000,
    });
  }

  openPreviewDialog() {
    let data = {
      title: `${this.studySet.title}`,
      content: `${this.studySet.description}`,
      studySet: this.studySet,
      actualUserId: this.actualUserId,
    };
    this.dialog.open(PreviewCardsComponent, {
      data: data,
      width: '80vw',
    });
  }
}
