import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StudySet } from 'src/app/interfaces/StudySet.interface';
import { StudySetsService } from 'src/app/services/study-sets.service';
import { Folder } from 'src/app/interfaces/Folder.interface';

interface DialogData {
  title: string;
  content: string;
  folder?: Folder;
  studySetsInFolder?: StudySet[];
}

@Component({
  selector: 'app-add-study-set-dialog',
  templateUrl: './add-study-set-dialog.component.html',
  styleUrls: ['./add-study-set-dialog.component.scss'],
})
export class AddStudySetDialogComponent implements OnInit {
  addStudySetForm: FormGroup;
  myStudySetsWithNoFolders: StudySet[];
  dataLoaded = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private formBuilder: FormBuilder,
    private studySetsService: StudySetsService
  ) {}

  ngOnInit(): void {
    this.addStudySetForm = this.formBuilder.group({
      search: ['', [Validators.required, Validators.minLength(2)]],
    });
    this.studySetsService.getUserStudySets().subscribe((userStudySets) => {      
      const alreadyInFolder = this.data.studySetsInFolder;      
      this.myStudySetsWithNoFolders = userStudySets.filter(
        ({ id: id1 }) => !alreadyInFolder.some(({ id: id2 }) => id2 === id1)
      );
      this.dataLoaded = true;      
    });
  }
}
