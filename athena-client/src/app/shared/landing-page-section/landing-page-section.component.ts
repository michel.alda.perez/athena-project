import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-landing-page-section',
  templateUrl: './landing-page-section.component.html',
  styleUrls: ['./landing-page-section.component.scss'],
})
export class LandingPageSectionComponent implements OnInit {
  @Input() imageUrl: string;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() buttonMessage: string;
  @Input() buttonAction: string;
  @Input() buttonColor: string;
  @Input() noImage: boolean;
  constructor() {}

  ngOnInit(): void {}
}
