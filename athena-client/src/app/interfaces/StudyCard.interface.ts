export interface StudyCard {
  id: string;
  term: string;
  definition: string;
  status: string;
  card_image?: string;
}

export const cardsTest: StudyCard[] = [
  {
    id: 'f5ef17c6-ba54-4a96-9a29-da84ed5e2d48',
    term: 'Cthulhu',
    definition: 'The Great Dreamer',
    status: 'untrained',
    card_image: null,
  },
  {
    id: '25426c44-a23e-4a5b-ab47-ee2661f773a9',
    term: 'Achilles',
    definition: 'Hero Of The Trojan War',
    status: 'untrained',
    card_image: null,
  },
  {
    id: '3a4403c6-9f29-4b1b-9ba4-513244536fea',
    term: 'Agni',
    definition: 'God Of Fire',
    status: 'untrained',
    card_image: null,
  },
  {
    id: '68a5f06c-4a6f-4758-8730-67c4b9071b0b',
    term: 'Ah Muzen Cab',
    definition: 'God Of Bees',
    status: 'untrained',
    card_image: null,
  },
  {
    id: 'ab6cb451-fd1b-4596-b93c-52348a030102',
    term: 'Ah Puch',
    definition: 'Horrifc God Of Decay',
    status: 'untrained',
    card_image: null,
  },
];
