import { StudySet } from './StudySet.interface';

export interface Folder {
  id: string;
  title: string;
  description: string;
  user: string;
  creation_date: string;
  study_set?: StudySet[];
}
