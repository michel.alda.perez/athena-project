import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';

interface Credentials {
  authenticated: boolean;
  user: any;
}
interface SignUpForm {
  username: string;
  email: string;
  password: string;
}

interface LoginForm {
  username: string;
  password: string;
}

interface Available {
  available: boolean;
}

interface User {
  id: string;
  username: string;
  full_name: string;
  email: string;
  profile_photo: string;
  password: string;
  username_changed: boolean;
  facebook_id: string;
  google_id: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  baseUrl = 'https://athena-project-app.herokuapp.com/auth';
  isAuthenticated = new BehaviorSubject(null);

  constructor(private http: HttpClient) {}

  getUser() {
    return this.http.get<User>(this.baseUrl + '/user');
  }

  getUserPhoto(id: string) {
    return this.http.get<User>(this.baseUrl + `/user/${id}`);
  }

  google() {
    window.open(`${this.baseUrl}/google`, '_self');
  }

  facebook() {
    window.open(`${this.baseUrl}/facebook`, '_self');
  }

  upload(image) {
    const formData: FormData = new FormData();
    formData.append('profile_photo', image, image.name);
    return this.http.post(this.baseUrl + '/upload', formData);
  }

  editUser(values) {
    return this.http.post(this.baseUrl + '/edit', values);
  }

  /**
   * Method to signup an user locally using own server
   * @param values interface SignUpInterface (username, email, password)
   * Returns an observable which in success sets authenticated true
   */
  singup(values: SignUpForm) {
    return this.http
      .post(this.baseUrl + '/signup', values)
      .pipe(tap((value) => this.isAuthenticated.next(true)));
  }

  /**
   * Method to log in an user locally using own server
   * @param values interface LoginInterface (username | email, password)
   * Returns an observable which in success sets authenticated true
   */
  login(values: LoginForm) {
    return this.http
      .post(this.baseUrl + '/login', values)
      .pipe(tap((value) => this.isAuthenticated.next(true)));
  }

  /**
   * Method to verify if a user is logged in returning true
   */
  isLoggedIn() {
    return this.http
      .get<Credentials>(this.baseUrl + '/loggedin')
      .pipe(
        tap(({ authenticated }) => this.isAuthenticated.next(authenticated))
      );
  }

  /**
   * Method to log out an user
   */
  logout() {
    return this.http
      .get<Credentials>(this.baseUrl + '/logout')
      .pipe(tap(({ authenticated }) => this.isAuthenticated.next(false)));
  }

  delete() {
    return this.http
      .get<Credentials>(this.baseUrl + '/delete')
      .pipe(
        tap(({ authenticated }) => this.isAuthenticated.next(authenticated))
      );
  }

  /**
   * Method to verify if an email exists in the database
   * @param email User email type string
   * Returns true if the email its available error otherwise
   */
  validateEmail(email: string) {
    return this.http.post<Available>(this.baseUrl + '/available/email', {
      email,
    });
  }

  /**
   * Method to verify if an username exists in the database
   * @param username User username type string
   * Returns true if the username its available error otherwise
   */
  validateUsername(username: string) {
    return this.http.post<Available>(this.baseUrl + '/available/username', {
      username,
    });
  }

  validateCredentials(values: LoginForm) {
    return this.http.post(this.baseUrl + '/validCredentials', {
      values,
    });
  }
}
