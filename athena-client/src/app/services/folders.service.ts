import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Folder } from '../interfaces/Folder.interface';
import { StudySet } from '../interfaces/StudySet.interface';

@Injectable({
  providedIn: 'root',
})
export class FoldersService {
  baseUrl = 'https://athena-project-app.herokuapp.com/api/folders';

  constructor(private http: HttpClient) {}

  getUserFolders() {
    return this.http.get<Folder[]>(this.baseUrl);
  }

  getFolder(id: string) {
    return this.http.get<Folder>(this.baseUrl + `/${id}`);
  }

  createFolder(formValues) {
    return this.http.post<Folder>(this.baseUrl + `/create`, formValues);
  }

  editFolder(id: string, formValues) {
    return this.http.post<Folder>(this.baseUrl + `/${id}/edit`, formValues);
  }

  deleteFolder(id: string) {
    return this.http.get(this.baseUrl + `/${id}/delete`);
  }

  addToFolder(id: string, study_set_id) {
    return this.http.post<StudySet>(this.baseUrl + `/${id}/add`, study_set_id);
  }

  removeFromFolder(id: string, study_set_id) {
    return this.http.post<StudySet>(
      this.baseUrl + `/${id}/remove`,
      study_set_id
    );
  }
}
