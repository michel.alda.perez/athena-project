import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StudyCard } from '../interfaces/StudyCard.interface';

@Injectable({
  providedIn: 'root',
})
export class StudyCardsService {
  baseUrl = 'https://athena-project-app.herokuapp.com/api/study_cards';

  constructor(private http: HttpClient) {}

  getCard(id: string) {
    return this.http.get<StudyCard>(this.baseUrl + `/${id}`);
  }

  createCard(formValues) {
    return this.http.post<StudyCard>(this.baseUrl + '/create', formValues);
  }

  editCard(id: string, formValues) {
    return this.http.post<StudyCard>(this.baseUrl + `/${id}/edit`, formValues);
  }

  deleteCard(id: string) {
    return this.http.get(this.baseUrl + `/${id}/delete`);
  }
}
