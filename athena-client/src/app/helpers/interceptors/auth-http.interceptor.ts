import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpEventType,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(this.addCookie(req));
  }

  private addCookie(req: HttpRequest<any>) {
    // if (!req.url.match(/https\:\/\/athena\-project\-app\.herokuapp\.com/)) {
    //   return req;
    // }
    return req.clone({ withCredentials: true });
  }
}
