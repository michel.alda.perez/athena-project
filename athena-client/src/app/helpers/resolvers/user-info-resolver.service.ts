import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';

import { User } from '../../interfaces/User.interface';
import { AuthService } from '../../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class UserInfoResolverService implements Resolve<User> {
  constructor(private authService: AuthService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): User | Observable<User> | Promise<User> {
    return this.authService.getUser();
  }
}
