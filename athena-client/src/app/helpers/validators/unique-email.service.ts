import { Injectable } from '@angular/core';
import { AsyncValidator, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UniqueEmailService implements AsyncValidator {
  constructor(private authService: AuthService) {}

  validate = (control: FormControl) => {
    const { value } = control;
    return this.authService.validateEmail(value).pipe(
      map((resp) => {
        return resp.available === true ? null : { emailNotAvailable: true };
      })
    );
  };
}
