import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../shared/dialog/dialog.component';

import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss'],
})
export class LogInComponent implements OnInit {
  logInForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.logInForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  handleForm() {
    if (this.logInForm.invalid) {
      Object.values(this.logInForm.controls).forEach((control) => {
        control.markAsDirty();
        control.markAsTouched();
      });
    } else {
      this.authService.login(this.logInForm.value).subscribe({
        next: (res) => {
          return this.router.navigateByUrl('/home');
        },
        error: ({ error }) => {
          this.openDialogInvalidCredentials();
        },
      });
    }
  }

  openDialogInvalidCredentials() {
    let dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'Invalid Credentials',
        content: 'Please verify your credentials to continue',
      },
    });
    dialogRef.afterClosed().subscribe();
  }

  googleLogin() {
    this.authService.google();
  }

  facebookLogin() {
    this.authService.facebook();
  }
}
