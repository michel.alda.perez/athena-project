import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { UniqueEmailService } from '../../helpers/validators/unique-email.service';
import { UniqueUsernameService } from '../../helpers/validators/unique-username.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  singUpForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private uniqueEmailService: UniqueEmailService,
    private uniqueUsernameService: UniqueUsernameService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.singUpForm = this.formBuilder.group({
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(23),
          Validators.pattern('^[a-zA-Z0-9]+$'),
        ],
        [this.uniqueUsernameService.validate],
      ],
      email: [
        '',
        [Validators.required, Validators.email],
        [this.uniqueEmailService.validate],
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(23),
        ],
      ],
    });
  }

  handleForm() {
    if (this.singUpForm.invalid) {
      Object.values(this.singUpForm.controls).forEach((control) => {
        control.markAsDirty();
        control.markAsTouched();
      });
    } else {
      this.authService.singup(this.singUpForm.value).subscribe({
        next: (res: any) => {          
          if (res.authenticated === true) {            
            this.router.navigateByUrl('/home');
          }
        },
        error: (err) => {
          console.log('Signup Error: ', err);
        },
      });
    }
  }
  googleLogin() {
    this.authService.google();
  }
}
