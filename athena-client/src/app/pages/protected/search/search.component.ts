import { Component, OnInit } from '@angular/core';
import { StudySet } from 'src/app/interfaces/StudySet.interface';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/interfaces/User.interface';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  studySets: StudySet[];
  searchForm: FormGroup;
  showImage = true;
  user: User;
  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      search: ['', [Validators.required, Validators.minLength(3)]],
    });
    this.authService.getUser().subscribe((user) => {
      this.user = user;
    });
  }

  handleSearch() {
    if (this.searchForm.valid) {
      this.activatedRoute.data.subscribe((data) => {
        this.studySets = data.studySets;
      });
      this.showImage = false;
    } else {
      this.showImage = true;
      this.studySets = [];
    }
  }

  getErrorMessage(): string {
    if (this.searchForm.get('search').hasError('required'))
      return 'You must enter a value';
    if (this.searchForm.get('search').hasError('minlength'))
      return 'To search insert at least 3 characters';
  }
}
