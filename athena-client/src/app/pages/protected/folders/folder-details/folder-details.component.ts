import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Folder } from 'src/app/interfaces/Folder.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/interfaces/User.interface';
import { MatDialog } from '@angular/material/dialog';
import { EditFolderComponent } from 'src/app/shared/edit-dialogs/edit-folder/edit-folder.component';
import { FoldersService } from 'src/app/services/folders.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { AddStudySetDialogComponent } from 'src/app/shared/add-study-set-dialog/add-study-set-dialog.component';
import { StudySetsService } from 'src/app/services/study-sets.service';
import { StudySet } from 'src/app/interfaces/StudySet.interface';

@Component({
  selector: 'app-folder-details',
  templateUrl: './folder-details.component.html',
  styleUrls: ['./folder-details.component.scss'],
})
export class FolderDetailsComponent implements OnInit {
  currentFolder: Folder;
  user: User;
  constructor(
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private changeDetector: ChangeDetectorRef,
    private foldersService: FoldersService,
    private studySetsService: StudySetsService,
    private _snackbar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data) => {
      this.currentFolder = data.currentFolder;
      this.user = data.user;
    });
  }

  openEditFolderDialog() {
    let dialogRef = this.dialog.open(EditFolderComponent, {
      data: {
        title: `Edit "${this.currentFolder.title}"`,
        content: `Edit your folder information`,
        folder: this.currentFolder,
        isEditing: true,
      },
      width: '80vh',
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res !== 'false' && res !== undefined) {
        this.currentFolder.title = res.title;
        this.currentFolder.description = res.description;
        this.changeDetector.detectChanges();
        this.foldersService
          .editFolder(this.currentFolder.id, res)
          .subscribe((res) => {
            this._snackbar.open('Folder Edited', 'Successfully', {
              duration: 2000,
            });
          });
      }
    });
  }

  openDeleteFolderDialog() {
    let dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: `Delete "${this.currentFolder.title}"`,
        content: `This process can't be undone, if the folder has study sets they also will be deleted. Do you want to continue?`,
      },
      width: '80vh',
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res === 'true') {
        this.foldersService
          .deleteFolder(this.currentFolder.id)
          .subscribe((res) => {
            this.router.navigateByUrl('/home/folders');
            this._snackbar.open('Folder Deleted', 'Successfully', {
              duration: 2000,
            });
          });
      }
    });
  }

  openAddToFolderDialog() {
    let dialogRef = this.dialog.open(AddStudySetDialogComponent, {
      data: {
        title: `Add to "${this.currentFolder.title}"`,
        content: `Add an study set to a folder`,
        studySetsInFolder: this.currentFolder.study_set,
      },
      width: '80vh',
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res !== 'false' && res !== undefined && res !== false) {
        let studySetAdded;
        this.studySetsService.getOneStudySet(res.id).subscribe((studySet) => {
          studySetAdded = studySet;
          this.foldersService
            .addToFolder(this.currentFolder.id, { study_set_id: res.id })
            .subscribe((res) => {
              this.currentFolder.study_set.push(studySetAdded);
              this.changeDetector.detectChanges();
            });
        });
      }
    });
  }

  openRemoveStudySetDialog(studySet: StudySet, index: number) {
    let dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: `Remove ${studySet.title}`,
        content: `Do you want to remove '${studySet.title}' from '${this.currentFolder.title}' ? `,
      },
      width: '80vw',
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res === 'true') {
        let temp = this.currentFolder.study_set[index];
        this.currentFolder.study_set.splice(index, 1);
        this.changeDetector.detectChanges();
        this.foldersService
          .removeFromFolder(this.currentFolder.id, {
            study_set_id: studySet.id,
          })
          .subscribe(() => {
            this._snackbar.open('Study Set Removed', 'Successfully', {
              duration: 2000,
            });
          });
      }
    });
  }
}
