import { Component, OnInit } from '@angular/core';
import { Folder } from 'src/app/interfaces/Folder.interface';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-folders',
  templateUrl: './folders.component.html',
  styleUrls: ['./folders.component.scss'],
})
export class FoldersComponent implements OnInit {
  myFolders: Folder[];
  foldersLoaded: boolean;
  constructor(private activatedRoute: ActivatedRoute) {
    this.foldersLoaded = false;
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data) => {
      this.myFolders = data.myFolders;
      this.foldersLoaded = true;      
    });
  }
}
