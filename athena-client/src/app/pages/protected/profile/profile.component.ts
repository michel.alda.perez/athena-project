import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/User.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UniqueUsernameService } from 'src/app/helpers/validators/unique-username.service';
import { UniqueEmailService } from 'src/app/helpers/validators/unique-email.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  user: User;
  dataLoaded = false;
  toggle = false;
  profileForm: FormGroup;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private uniqueEmailService: UniqueEmailService,
    private uniqueUsernameService: UniqueUsernameService,
    private _snackBar: MatSnackBar,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      username: [
        { value: '', disabled: true },
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(23),
          Validators.pattern('^[a-zA-Z0-9]+$'),
        ],
        [this.uniqueUsernameService.validate],
      ],
      full_name: [{ value: '', disabled: true }],
      email: [
        { value: '', disabled: true },
        [Validators.required, Validators.email],
        this.uniqueEmailService.validate,
      ],
      password: [{ value: '', disabled: true }],
      profile_photo: [{ value: '', disabled: true }],
    });

    this.activatedRoute.data.subscribe((data) => {
      this.user = data.user;
      this.profileForm.patchValue({
        username: this.user.username,
        email: this.user.email,
        full_name: this.user.full_name,
        profile_photo: this.user.profile_photo,
      });
      this.dataLoaded = true;
    });
  }

  enableEdit() {
    this.toggle = !this.toggle;
    if (this.toggle) {
      if (!this.user.username_changed) {
        this.profileForm.get('username').enable();
      }
      this.profileForm.get('email').enable();
      this.profileForm.get('password').enable();
      this.profileForm.get('full_name').enable();
      this.profileForm.get('profile_photo').enable();
    } else {
      this.profileForm.get('username').disable();
      this.profileForm.get('email').disable();
      this.profileForm.get('password').disable();
      this.profileForm.get('full_name').disable();
      this.profileForm.get('profile_photo').disable();
    }
  }

  previewPhoto(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.profileForm.patchValue({
        profile_photo: file,
      });
      const reader = new FileReader();
      reader.onload = () => {
        this.user.profile_photo = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  openInput() {
    document.getElementById('fileInput').click();
  }

  handleForm() {
    if (this.profileForm.invalid) {
      Object.values(this.profileForm.controls).forEach((control) => {
        control.markAsDirty();
        control.markAsTouched();
      });
      return;
    } else {
      this.enableEdit();
      const { profile_photo } = this.profileForm.value;
      if (profile_photo !== this.user.profile_photo && profile_photo !== '') {
        this.authService.upload(profile_photo).subscribe();
      }
      this.authService.editUser(this.profileForm.value).subscribe();
    }
  }

  resetForm() {
    this.enableEdit();
    this.profileForm.reset({
      username: this.user.username,
      email: this.user.email,
      full_name: this.user.full_name,
      password: '',
    });
    this.profileForm.patchValue({
      profile_photo: this.user.profile_photo,
    });
  }

  deleteAccount() {
    this.authService.delete().subscribe(() => {
      this.router.navigateByUrl('/login');
    });
    this._snackBar.open('Delete Account', 'Successfully', { duration: 2000 });
  }

  openDialog() {
    let dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'Delete Account Confirmation',
        content: `All your data will be lost, this can't be undone. Do you want to continue?`,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'true') {
        this.deleteAccount();
      }
    });
  }
}
