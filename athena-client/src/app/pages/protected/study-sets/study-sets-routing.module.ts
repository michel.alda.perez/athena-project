import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudySetsComponent } from './study-sets.component';
import { StudySetCardsComponent } from './study-set-cards/study-set-cards.component';
import { StudySetDetailsComponent } from './study-set-details/study-set-details.component';
import { StudySetsUserResolverService } from '../../../helpers/resolvers/study-sets-user-resolver.service';
import { StudySetOneResolverService } from '../../../helpers/resolvers/study-set-one-resolver.service';
import { UserInfoResolverService } from 'src/app/helpers/resolvers/user-info-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: StudySetsComponent,
    resolve: {
      myStudySets: StudySetsUserResolverService,
      user: UserInfoResolverService,
    },
  },
  {
    path: ':id',
    component: StudySetDetailsComponent,
    resolve: {
      studySet: StudySetOneResolverService,
      user: UserInfoResolverService,
    },
    runGuardsAndResolvers: 'always',
  },
  {
    path: ':id/cards',
    component: StudySetCardsComponent,
    resolve: { studySet: StudySetOneResolverService },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudySetsRoutingModule {}
