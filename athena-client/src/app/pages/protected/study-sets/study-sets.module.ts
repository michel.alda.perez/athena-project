import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudySetsRoutingModule } from './study-sets-routing.module';
import { StudySetsComponent } from './study-sets.component';
import { SharedModule } from 'src/app/shared/shared.module';

import { StudySetCardsComponent } from './study-set-cards/study-set-cards.component';
import { StudySetDetailsComponent } from './study-set-details/study-set-details.component';

@NgModule({
  declarations: [StudySetsComponent, StudySetCardsComponent, StudySetDetailsComponent],
  imports: [CommonModule, StudySetsRoutingModule, SharedModule],
})
export class StudySetsModule {}
