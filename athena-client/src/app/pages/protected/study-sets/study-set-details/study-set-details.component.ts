import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { StudySet } from 'src/app/interfaces/StudySet.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/interfaces/User.interface';
import { StudyCard } from 'src/app/interfaces/StudyCard.interface';
import { EditCardComponent } from 'src/app/shared/edit-dialogs/edit-card/edit-card.component';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { EditStudySetComponent } from 'src/app/shared/edit-dialogs/edit-study-set/edit-study-set.component';
import { StudySetsService } from 'src/app/services/study-sets.service';
import { StudyCardsService } from 'src/app/services/study-cards.service';

@Component({
  selector: 'app-study-set-details',
  templateUrl: './study-set-details.component.html',
  styleUrls: ['./study-set-details.component.scss'],
})
export class StudySetDetailsComponent implements OnInit {
  currentStudySet: StudySet;
  cardsOfCurrentStudy: StudyCard[];
  currentCardTest: StudyCard;
  user: User;
  change: string;
  originalUserPhoto: string;
  allLoaded = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private studySetsService: StudySetsService,
    private studyCardsService: StudyCardsService,
    private router: Router,
    private changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data) => {
      this.currentStudySet = data.studySet;
      this.cardsOfCurrentStudy = this.currentStudySet.study_cards;
      this.user = data.user;
    });
  }

  openCreateCardDialog() {
    let dialogRef = this.dialog.open(EditCardComponent, {
      data: {
        title: `Create card for "${this.currentStudySet.title}"`,
        content: `Write a term and his definition`,
        studySet: this.currentStudySet,
        createCard: true,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result !== 'false' && result !== undefined) {
        this.studyCardsService.createCard(result).subscribe((res) => {
          this.cardsOfCurrentStudy.push(res);
          this.changeDetector.detectChanges();
          this.saveCreatedCard();
        });
      }
    });
  }

  saveCreatedCard() {
    this._snackBar.open(`Card Created`, 'Successfully', {
      duration: 2000,
    });
  }

  openEditStudySetDialog() {
    let dialogRef = this.dialog.open(EditStudySetComponent, {
      data: {
        title: `Edit "${this.currentStudySet.title}"`,
        content: 'Edit your study set information',
        studySet: this.currentStudySet,
        editing: true,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result !== 'false' && result !== undefined) {
        this.currentStudySet.title = result.title;
        this.currentStudySet.description = result.description;
        this.currentStudySet.visibility = result.visibility;
        this.changeDetector.detectChanges();
        this.studySetsService
          .editStudySet(this.currentStudySet.id, result)
          .subscribe(() => this.editStudySet());
      }
    });
  }

  editStudySet() {
    this._snackBar.open(
      `"${this.currentStudySet.title}" Edited`,
      'Successfully',
      { duration: 2000 }
    );
  }

  openDeleteDialog() {
    let dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: `Delete "${this.currentStudySet.title}"`,
        content: `Delete "${this.currentStudySet.title}" and all his content?, this proccess can't be undone.`,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'true') {
        this.deleteStudySet(this.currentStudySet.id);
      }
    });
  }

  deleteStudySet(studySetId: string) {
    this._snackBar.open(
      `"${this.currentStudySet.title}" Deleted`,
      'Successfully',
      { duration: 2000 }
    );
    this.studySetsService.deleteStudySet(studySetId).subscribe(() => {
      this.router.navigateByUrl('/home/study-sets');
    });
  }

  openEditCardDialog(cardId: string) {
    const card: StudyCard = this.cardsOfCurrentStudy[cardId];
    let dialogRef = this.dialog.open(EditCardComponent, {
      data: {
        title: `Edit "Card" `,
        content: `Edit term and definition`,
        study_set: this.currentStudySet,
        card,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result !== 'false' && result !== undefined) {
        this.cardsOfCurrentStudy[cardId].term = result.term;
        this.cardsOfCurrentStudy[cardId].definition = result.definition;
        this.changeDetector.detectChanges();
        this.studyCardsService
          .editCard(card.id, result)
          .subscribe(() => this.saveEditedCard());
      }
    });
  }

  saveEditedCard() {
    this._snackBar.open(`Card Edited`, 'Successfully', {
      duration: 2000,
    });
  }

  openDeleteCardDialog(index: number) {
    const cardToDelete: StudyCard = this.cardsOfCurrentStudy[index];
    let dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: `Delete Card '${cardToDelete.term}'`,
        content: `Delete this card and all his content?, this proccess can't be undone`,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'true') {
        this.cardsOfCurrentStudy[index].term = '';
        this.cardsOfCurrentStudy[index].definition = '';
        this.changeDetector.detectChanges();
        this.studyCardsService.deleteCard(cardToDelete.id).subscribe(() => {
          this.deleteCard();
        });
      }
    });
  }

  deleteCard() {
    this._snackBar.open(`Card`, 'Deleted Successfully', {
      duration: 2000,
    });
  }
}
