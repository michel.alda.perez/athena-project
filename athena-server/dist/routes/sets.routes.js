"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var sets_controller_1 = require("../controllers/sets.controller");
var router = express_1.Router();
// Get all public study sets
router.get('/study_sets', sets_controller_1.getStudySets);
// Get all study sets of user
router.get('/study_sets/user', sets_controller_1.getUserStudySets);
// Get specific study set
router.get('/study_sets/:id', sets_controller_1.getStudySet);
// Create a new study set
router.post('/study_sets/create', sets_controller_1.getCreateStudySet);
// Save a study set from other user
router.get('/study_sets/:id/save', sets_controller_1.getSaveStudySet);
// Edit an study set
router.post('/study_sets/:id/edit', sets_controller_1.postEditStudySet);
// Delete an study set
router.get('/study_sets/:id/delete', sets_controller_1.getDeleteStudySet);
exports.default = router;
