"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var cards_controller_1 = require("../controllers/cards.controller");
var router = express_1.Router();
// Get specific card
router.get('/study_cards/:id', cards_controller_1.getCard);
// Create a new card
router.post('/study_cards/create', cards_controller_1.postCreateCard);
// Edit specific card
router.post('/study_cards/:id/edit', cards_controller_1.postEditCard);
// Delete specific card
router.get('/study_cards/:id/delete', cards_controller_1.getDeleteCard);
exports.default = router;
