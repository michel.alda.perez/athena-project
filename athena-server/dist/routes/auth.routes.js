"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var passport_config_1 = __importDefault(require("../config/passport.config"));
var auth_controller_1 = require("../controllers/auth.controller");
var cloudinary_config_1 = __importDefault(require("../config/cloudinary.config"));
var router = express_1.Router();
// Check if user is logged in
router.get('/loggedin', auth_controller_1.loggedin);
// Get User Info
router.get('/user', auth_controller_1.getUser);
// Get User Photo
router.get('/user/:id', auth_controller_1.getUserPhoto);
// Sing up an user
router.post('/signup', auth_controller_1.postSignup);
// Log in an user
router.post('/login', passport_config_1.default.authenticate('local'), auth_controller_1.postLogin);
// Edit user
router.post('/edit', auth_controller_1.postEdit);
// Upload profile photo
router.post('/upload', cloudinary_config_1.default.single('profile_photo'), auth_controller_1.postUpload);
// Check if email is available to use
router.post('/available/email', auth_controller_1.verifyEmail);
// Check if username is available to use
router.post('/available/username', auth_controller_1.verifyUsername);
// Log out an user
router.get('/logout', auth_controller_1.logout);
// Delete an user
router.get('/delete', auth_controller_1.getDelete);
// Google Authentication
router.get('/google', passport_config_1.default.authenticate('google', {
    scope: ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email'],
}));
router.get('/google/callback', passport_config_1.default.authenticate('google', {
    failureRedirect: "" + process.env.ENDPOINT,
}), function (req, res) {
    res.redirect(process.env.ENDPOINT + "/home?authenticated=true");
});
// Facebook Authentication
router.get('/facebook', passport_config_1.default.authenticate('facebook', {
    scope: ['email'],
}));
router.get('/facebook/callback', passport_config_1.default.authenticate('facebook', {
    failureRedirect: "" + process.env.ENDPOINT,
}), function (req, res) {
    res.redirect(process.env.ENDPOINT + "/home?authenticated=true");
});
exports.default = router;
