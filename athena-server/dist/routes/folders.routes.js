"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var folders_controller_1 = require("../controllers/folders.controller");
var router = express_1.Router();
// Get all the folders from user
router.get('/folders/', folders_controller_1.getFolders);
// Get specific folder from user folders
router.get('/folders/:id', folders_controller_1.getFolder);
// Create a new folder
router.post('/folders/create', folders_controller_1.postCreateFolder);
// Edit a folder
router.post('/folders/:id/edit', folders_controller_1.postEditFolder);
// Add study set to specific folder
router.post('/folders/:id/add', folders_controller_1.postAddToFolder);
// Remove study set to specific folder
router.post('/folders/:id/remove', folders_controller_1.postRemoveFromFolder);
// Delete a folder
router.get('/folders/:id/delete', folders_controller_1.getDeleteFolder);
exports.default = router;
