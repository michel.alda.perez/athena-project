"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
var typeorm_1 = require("typeorm");
var Folder_entity_1 = require("./Folder.entity");
var StudySet_entity_1 = require("./StudySet.entity");
var User = /** @class */ (function () {
    function User() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn('uuid'),
        __metadata("design:type", String)
    ], User.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], User.prototype, "username", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], User.prototype, "full_name", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], User.prototype, "email", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], User.prototype, "password", void 0);
    __decorate([
        typeorm_1.Column({
            default: 'https://res.cloudinary.com/dbc1hdxqi/image/upload/v1595354981/athena-project/assets/athena-default-icon_f4swin.png',
        }),
        __metadata("design:type", String)
    ], User.prototype, "profile_photo", void 0);
    __decorate([
        typeorm_1.Column({ default: false }),
        __metadata("design:type", Boolean)
    ], User.prototype, "username_changed", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], User.prototype, "google_id", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], User.prototype, "facebook_id", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Folder_entity_1.Folder; }, function (folder) { return folder.user; }),
        __metadata("design:type", Array)
    ], User.prototype, "folders", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return StudySet_entity_1.StudySet; }, function (study_set) { return study_set.user; }),
        __metadata("design:type", Array)
    ], User.prototype, "study_sets", void 0);
    User = __decorate([
        typeorm_1.Entity(),
        typeorm_1.Index('email_user', ['email']),
        typeorm_1.Index('username_user', ['username'])
    ], User);
    return User;
}());
exports.User = User;
