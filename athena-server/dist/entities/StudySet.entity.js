"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StudySet = void 0;
var typeorm_1 = require("typeorm");
var StudyCard_entity_1 = require("./StudyCard.entity");
var Folder_entity_1 = require("./Folder.entity");
var User_entity_1 = require("./User.entity");
var StudySet = /** @class */ (function () {
    function StudySet() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn('uuid'),
        __metadata("design:type", String)
    ], StudySet.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], StudySet.prototype, "title", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], StudySet.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column({ default: 'public' }),
        __metadata("design:type", String)
    ], StudySet.prototype, "visibility", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Date)
    ], StudySet.prototype, "creation_date", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", Date)
    ], StudySet.prototype, "completation_date", void 0);
    __decorate([
        typeorm_1.Column({ default: 0.0 }),
        __metadata("design:type", Number)
    ], StudySet.prototype, "grade", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], StudySet.prototype, "original_user_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return User_entity_1.User; }, function (user) { return user.study_sets; }, {
            cascade: true,
            onDelete: 'CASCADE',
        }),
        __metadata("design:type", User_entity_1.User)
    ], StudySet.prototype, "user", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Folder_entity_1.Folder; }, function (folder) { return folder.study_set; }, {
            cascade: true,
            onDelete: 'CASCADE',
        }),
        __metadata("design:type", Folder_entity_1.Folder)
    ], StudySet.prototype, "folder", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return StudyCard_entity_1.StudyCard; }, function (study_card) { return study_card.study_set; }),
        __metadata("design:type", Array)
    ], StudySet.prototype, "study_cards", void 0);
    StudySet = __decorate([
        typeorm_1.Entity()
    ], StudySet);
    return StudySet;
}());
exports.StudySet = StudySet;
