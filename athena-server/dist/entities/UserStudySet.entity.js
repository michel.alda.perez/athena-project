"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserStudySet = void 0;
var typeorm_1 = require("typeorm");
var User_entity_1 = require("./User.entity");
var StudySet_entity_1 = require("./StudySet.entity");
var UserStudySet = /** @class */ (function () {
    function UserStudySet() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", String)
    ], UserStudySet.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], UserStudySet.prototype, "original_user_id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], UserStudySet.prototype, "user_id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], UserStudySet.prototype, "study_set_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return User_entity_1.User; }, function (user) { return user.user_study_sets; }),
        __metadata("design:type", User_entity_1.User)
    ], UserStudySet.prototype, "user", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return StudySet_entity_1.StudySet; }, function (study_set) { return study_set.user_study_sets; }),
        __metadata("design:type", StudySet_entity_1.StudySet)
    ], UserStudySet.prototype, "study_set", void 0);
    UserStudySet = __decorate([
        typeorm_1.Entity()
    ], UserStudySet);
    return UserStudySet;
}());
exports.UserStudySet = UserStudySet;
