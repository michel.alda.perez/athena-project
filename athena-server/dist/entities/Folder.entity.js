"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Folder = void 0;
var typeorm_1 = require("typeorm");
var User_entity_1 = require("./User.entity");
var StudySet_entity_1 = require("./StudySet.entity");
var Folder = /** @class */ (function () {
    function Folder() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn('uuid'),
        __metadata("design:type", String)
    ], Folder.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Folder.prototype, "title", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Folder.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column({ default: new Date() }),
        __metadata("design:type", Date)
    ], Folder.prototype, "creation_date", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return User_entity_1.User; }, function (user) { return user.folders; }, {
            cascade: true,
            onDelete: 'CASCADE',
        }),
        __metadata("design:type", User_entity_1.User)
    ], Folder.prototype, "user", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return StudySet_entity_1.StudySet; }, function (study_set) { return study_set.folder; }),
        __metadata("design:type", Array)
    ], Folder.prototype, "study_set", void 0);
    Folder = __decorate([
        typeorm_1.Entity()
    ], Folder);
    return Folder;
}());
exports.Folder = Folder;
