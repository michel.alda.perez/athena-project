"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDeleteStudySet = exports.postEditStudySet = exports.getSaveStudySet = exports.getCreateStudySet = exports.getStudySet = exports.getUserStudySets = exports.getStudySets = void 0;
var typeorm_1 = require("typeorm");
var StudySet_entity_1 = require("../entities/StudySet.entity");
var User_entity_1 = require("../entities/User.entity");
/**
 * Method to get all public study sets
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Return a json object than contains an array of study sets
 */
exports.getStudySets = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                    .find({
                    where: { visibility: 'public' },
                    loadRelationIds: true,
                    order: { title: 'DESC', original_user_id: 'DESC' },
                })
                    .then(function (study_sets) { return res.status(200).json(study_sets); })
                    .catch(function (error) { return res.status(404).json(error); })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
/**
 * Method to get only the study sets of the user
 * @param req any due to req.user
 * @param res Response
 * @param next NextFunction
 * Return a json object than contains an array of study sets of one user
 */
exports.getUserStudySets = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id;
    return __generator(this, function (_a) {
        if (req.user) {
            id = req.user.id;
            typeorm_1.getRepository(User_entity_1.User)
                .findOne(id, { relations: ['study_sets'], })
                .then(function (data) { return res.status(200).json(data.study_sets); })
                .catch(function (error) { return res.status(505).json(error); });
        }
        else {
            res.status(404).json({ message: 'Not user found' });
        }
        return [2 /*return*/];
    });
}); };
/**
 * Method to get a specific study set
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Return a json object of a Study Set
 */
exports.getStudySet = function (req, res, next) {
    var id = req.params.id;
    typeorm_1.getRepository(StudySet_entity_1.StudySet)
        .findOne(id, { relations: ['study_cards', 'user', 'folder'], })
        .then(function (study_set) { return res.status(200).json(study_set); })
        .catch(function (error) { return res.status(404).json(error); });
};
/**
 * Method to create a Study Set
 * @param req any due to user id is needed
 * @param res Response
 * @param next NextFunction
 * Return a json object than contains the study set created
 */
exports.getCreateStudySet = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, title, description, visibility, newStudySet;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                if (!req.user) return [3 /*break*/, 2];
                id = req.user.id;
                _a = req.body, title = _a.title, description = _a.description, visibility = _a.visibility;
                newStudySet = typeorm_1.getRepository(StudySet_entity_1.StudySet).create({
                    title: title,
                    description: description,
                    visibility: visibility,
                    creation_date: new Date(),
                    user: id,
                    original_user_id: id,
                });
                return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                        .save(newStudySet)
                        .then(function (study_set) { return res.status(200).json(study_set); })
                        .catch(function (error) { return res.status(505).json(error); })];
            case 1:
                _b.sent();
                _b.label = 2;
            case 2: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to save a study set from other user
 * @param req any due to user id is needed
 * @param res Response
 * @param next NextFunction
 * Return a json object than contains the study set saved
 */
exports.getSaveStudySet = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.user) return [3 /*break*/, 2];
                id_1 = req.user.id;
                return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                        .findOne(req.params.id, { relations: ['study_cards', 'user'] })
                        .then(function (studySetToSave) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User)
                                        .findOne(studySetToSave.user.id)
                                        .then(function (originalUser) { return __awaiter(void 0, void 0, void 0, function () {
                                        var studySetSaved;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    studySetSaved = typeorm_1.getRepository(StudySet_entity_1.StudySet).create({
                                                        title: studySetToSave.title,
                                                        description: studySetToSave.description,
                                                        creation_date: studySetToSave.creation_date,
                                                        study_cards: studySetToSave.study_cards,
                                                        original_user_id: originalUser.id,
                                                        user: id_1,
                                                        visibility: 'saved',
                                                    });
                                                    return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                                                            .save(studySetSaved)
                                                            .then(function (study_set) { return res.status(200).json(study_set); })
                                                            .catch(function (error) { return res.status(505).json(error); })];
                                                case 1:
                                                    _a.sent();
                                                    return [2 /*return*/];
                                            }
                                        });
                                    }); })
                                        .catch(function (error) { return res.status(505).json(error); })];
                                case 1:
                                    _a.sent();
                                    return [2 /*return*/];
                            }
                        });
                    }); })
                        .catch(function (error) { return res.status(505).json(error); })];
            case 1:
                _a.sent();
                _a.label = 2;
            case 2: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to edit a study set
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Return a json object than contains the edited study set
 */
exports.postEditStudySet = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, title, description, visibility, id;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, title = _a.title, description = _a.description, visibility = _a.visibility;
                id = req.params.id;
                return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                        .findOne(id, { relations: ['user'], loadRelationIds: true })
                        .then(function (editStudySet) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!(req.user.id === editStudySet.user)) return [3 /*break*/, 3];
                                    return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet).update(id, {
                                            title: title,
                                            description: description,
                                            visibility: visibility,
                                        })];
                                case 1:
                                    _a.sent();
                                    return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                                            .findOne(id, { loadRelationIds: true })
                                            .then(function (study_set) { return res.status(200).json(study_set); })
                                            .catch(function (error) { return res.status(505).json(error); })];
                                case 2:
                                    _a.sent();
                                    return [3 /*break*/, 4];
                                case 3:
                                    res.status(404).json({ message: 'Invalid action' });
                                    _a.label = 4;
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); })];
            case 1:
                _b.sent();
                return [2 /*return*/];
        }
    });
}); };
/**
 * Method to delete a study set
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Returns a message on success or error on failure
 */
exports.getDeleteStudySet = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                if (!req.user) return [3 /*break*/, 2];
                return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                        .findOne(id, {
                        relations: ['user'],
                        loadRelationIds: true,
                    })
                        .then(function (studySetToDelete) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!(req.user.id = studySetToDelete.user)) return [3 /*break*/, 2];
                                    return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                                            .delete(id)
                                            .then(function () { return res.status(200).json({ message: 'Study Set deleted' }); })
                                            .catch(function (error) { return res.status(505).json(error); })];
                                case 1:
                                    _a.sent();
                                    return [3 /*break*/, 3];
                                case 2:
                                    res.status(404).json({ message: 'Invalid action' });
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })
                        .catch(function () { return res.status(404).json({ message: 'Invalid action' }); })];
            case 1:
                _a.sent();
                _a.label = 2;
            case 2: return [2 /*return*/];
        }
    });
}); };
