"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyUsername = exports.verifyEmail = exports.getDelete = exports.loggedin = exports.postEdit = exports.postUpload = exports.logout = exports.postLogin = exports.postSignup = exports.getUserPhoto = exports.getUser = void 0;
var typeorm_1 = require("typeorm");
var bcrypt_1 = __importDefault(require("bcrypt"));
var User_entity_1 = require("../entities/User.entity");
/**
 * Method to retrieve an user information
 * @param req Request (any) due to req.user.id
 * @param res Response
 * @param next Next Function
 * Returns a json object with the user info on success
 * a message 'Not User Found' if not user was founded
 * otherwise an error with status 500u
 */
exports.getUser = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, user, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                if (!req.user) return [3 /*break*/, 2];
                id = req.user.id;
                console.log('getuser id: ', id);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne({ id: id })];
            case 1:
                user = _a.sent();
                return [2 /*return*/, res.status(200).json(user)];
            case 2: return [2 /*return*/, res.status(404).json({ message: 'Not User Found' })];
            case 3: return [3 /*break*/, 5];
            case 4:
                error_1 = _a.sent();
                res.status(500).json(error_1);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports.getUserPhoto = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, user, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                id = req.params.id;
                console.log('getuser photo id: ', id);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne({ id: id })];
            case 1:
                user = _a.sent();
                return [2 /*return*/, res.status(200).json({ profile_photo: user.profile_photo })];
            case 2:
                error_2 = _a.sent();
                return [2 /*return*/, res.status(500).json(error_2)];
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to sign up a new user, if email doesn't exist
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Returns the user and authenticated on success
 * An error 409 (conflic) if the user exists
 * An internal server error otherwise
 */
exports.postSignup = function (req, res, next) {
    var _a = req.body, username = _a.username, email = _a.email, password = _a.password;
    typeorm_1.getRepository(User_entity_1.User)
        .findOne({ email: email })
        .then(function (userFounded) {
        if (!userFounded) {
            var hashPassword = generatePassword(password);
            var newUser = typeorm_1.getRepository(User_entity_1.User).create({
                email: email,
                username: username,
                password: hashPassword,
            });
            typeorm_1.getRepository(User_entity_1.User)
                .save(newUser)
                .then(function (user) {
                req.login(user, function (err) {
                    if (err)
                        return next(err);
                });
                return res.status(200).json({ user: user, authenticated: true });
            })
                .catch(function (error) {
                return res.status(500).json({ message: 'Internal Server Error' });
            });
        }
        else {
            return res.status(409).json({ message: 'The email is already in use', authenticated: false });
        }
    })
        .catch(function (error) {
        return res.status(500).json({ message: 'Internal Server Error' });
    });
};
/**
 * Method to log in a user with passport (local)
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Returns the user and authenticated on success
 * Otherwise an error and invalidCredentials true
 */
exports.postLogin = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        try {
            res.status(200).send({ user: req.user, authenticated: true, invalidCredentials: false });
        }
        catch (error) {
            res.status(409).json({ invalidCredentials: true, authenticated: false });
        }
        return [2 /*return*/];
    });
}); };
/**
 * Method to log out an user, clearing the cookie and set authenticated false
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
exports.logout = function (req, res, next) {
    try {
        req.logOut();
        res.status(200).clearCookie('connect.sid', {});
        req.session.destroy(function () {
            res.json({ authenticated: false });
        });
    }
    catch (error) {
        return res.status(500).json({ message: 'Internal Server Error' });
    }
};
/**
 * Method that uses cloudinary and multer to upload profile photo
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns a json object with the user updated on success
 * Otherwise an 'Internal Server Error' with status 500
 */
exports.postUpload = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var profile_photo, id, userToUpdate, user, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                profile_photo = req.body.profile_photo;
                id = req.user.id;
                userToUpdate = {};
                userToUpdate['profile_photo'] = req.file ? req.file.path : profile_photo;
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).update(id, {
                        profile_photo: userToUpdate['profile_photo'],
                    })];
            case 1:
                user = _a.sent();
                return [2 /*return*/, user ? res.status(200).json(user) : res.status(500).json({ message: 'Internal Server Error' })];
            case 2:
                error_3 = _a.sent();
                res.status(500).json(error_3);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to edit the user info, username can only be changed once
 * @param req Request any due to req.user.id
 * @param res Response
 * @param next NextFunction
 */
exports.postEdit = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, username, email, password, full_name, userToEdit, isMatch, user, error_4, error_5;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _b.trys.push([0, 6, , 7]);
                id = req.user.id;
                _a = req.body, username = _a.username, email = _a.email, password = _a.password, full_name = _a.full_name;
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne(id)];
            case 1:
                userToEdit = _b.sent();
                if (!userToEdit) return [3 /*break*/, 5];
                if (password !== '') {
                    isMatch = bcrypt_1.default.compareSync(password, userToEdit.password);
                    if (!isMatch)
                        userToEdit.password = generatePassword(password);
                }
                if (userToEdit.username_changed === false) {
                    if (username !== '' && username !== userToEdit.username) {
                        userToEdit.username_changed = true;
                        userToEdit.username = username;
                    }
                }
                userToEdit.email = email;
                userToEdit.full_name = full_name;
                _b.label = 2;
            case 2:
                _b.trys.push([2, 4, , 5]);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).save(userToEdit)];
            case 3:
                user = _b.sent();
                return [2 /*return*/, user ? res.status(200).json(user) : res.status(500).json({ message: 'internal Server Error' })];
            case 4:
                error_4 = _b.sent();
                return [2 /*return*/, res.status(500).json({ message: 'internal Server Error' })];
            case 5: return [3 /*break*/, 7];
            case 6:
                error_5 = _b.sent();
                return [2 /*return*/, res.status(500).json({ message: 'internal Server Error' })];
            case 7: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to verify if user is logged in the website
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns authenticated true and the user object if true otherwise authenticated false
 */
exports.loggedin = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                if (!req.user) return [3 /*break*/, 2];
                id = req.user.id;
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne(id)];
            case 1:
                _a.sent();
                return [2 /*return*/, res.status(200).json({ authenticated: true })];
            case 2: return [2 /*return*/, res.status(200).json({ authenticated: false })];
            case 3: return [3 /*break*/, 5];
            case 4:
                error_6 = _a.sent();
                res.status(500).json({ message: 'Internal Server Error' });
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to delete an user
 * @param req any due to req.user
 * @param res Response
 * @param next NextFunction
 * Returns authenticated false and response on delete
 */
exports.getDelete = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, userFounded_1, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                if (!req.user) return [3 /*break*/, 2];
                id = req.user.id;
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne(id)];
            case 1:
                userFounded_1 = _a.sent();
                if (userFounded_1) {
                    req.logOut();
                    res.clearCookie('connect.sid', {});
                    req.session.destroy(function () { return __awaiter(void 0, void 0, void 0, function () {
                        var error_8;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, , 3]);
                                    return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).delete(userFounded_1)];
                                case 1:
                                    _a.sent();
                                    return [2 /*return*/, res.status(200).json({ authenticated: false })];
                                case 2:
                                    error_8 = _a.sent();
                                    return [2 /*return*/, res.status(500).json(error_8)];
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); });
                }
                else {
                    return [2 /*return*/, res.status(404).json({ message: 'Not User Found' })];
                }
                _a.label = 2;
            case 2: return [3 /*break*/, 4];
            case 3:
                error_7 = _a.sent();
                return [2 /*return*/, res.status(500).json(error_7)];
            case 4: return [2 /*return*/];
        }
    });
}); };
/**
 * Verify if email is already in the DB
 * @param req Any due to req.user.email
 * @param res Response
 * @param next NextFunction
 * Returns available that check if the email has been register in the database
 * If an user is loggedin returns true if the actual email is the same
 *  to avoid unique email validator in edit form
 * Otherwise returns true or false if email is or not available
 */
exports.verifyEmail = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var email, actualEmail, user, error_9;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                email = req.body.email;
                if (req.user) {
                    actualEmail = req.user.email;
                    if (email === actualEmail) {
                        return [2 /*return*/, res.status(200).json({ available: true })];
                    }
                }
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne({ email: email })];
            case 1:
                user = _a.sent();
                !user ? res.status(200).json({ available: true }) : res.status(200).json({ available: false });
                return [3 /*break*/, 3];
            case 2:
                error_9 = _a.sent();
                res.status(500).json(error_9);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to verify is username is already in the DB
 * @param req any due to req.user.username
 * @param res Response
 * @param next NextFunction
 * Returns available that check if the username has been register in the database
 * If an user is loggedin returns true if the actual username is the same
 *  to avoid unique email validator in edit form
 * Otherwise returns true or false if username is or not available
 */
exports.verifyUsername = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var username, actualUsername, user, error_10;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                username = req.body.username;
                if (req.user) {
                    actualUsername = req.user.username;
                    if (username === actualUsername) {
                        return [2 /*return*/, res.status(200).json({ available: true })];
                    }
                }
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne({ username: username })];
            case 1:
                user = _a.sent();
                !user ? res.status(200).json({ available: true }) : res.status(200).json({ available: false });
                return [3 /*break*/, 3];
            case 2:
                error_10 = _a.sent();
                res.status(500).json(error_10);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
// Auxiliar Functions
/**
 * Method to generate a hashed password
 * @param password string password
 * Returns a hashed password
 */
var generatePassword = function (password) {
    var salt = bcrypt_1.default.genSaltSync(10);
    var hashPassword = bcrypt_1.default.hashSync(password, salt);
    return hashPassword;
};
