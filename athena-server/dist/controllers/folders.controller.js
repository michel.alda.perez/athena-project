"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDeleteFolder = exports.postRemoveFromFolder = exports.postAddToFolder = exports.postEditFolder = exports.postCreateFolder = exports.getFolder = exports.getFolders = void 0;
var typeorm_1 = require("typeorm");
var Folder_entity_1 = require("../entities/Folder.entity");
var User_entity_1 = require("../entities/User.entity");
var StudySet_entity_1 = require("../entities/StudySet.entity");
/**
 * Method to get all folders from user
 * @param req any due to req.user
 * @param res Response
 * @param next NextFucntion
 * Returns an json object array of user folders in success
 */
exports.getFolders = function (req, res, next) {
    var id = req.user.id;
    typeorm_1.getRepository(User_entity_1.User)
        .findOne(id, { relations: ['folders'] })
        .then(function (data) { return res.status(200).json(data.folders); })
        .catch(function (error) { return res.status(505).json(error); });
};
/**
 * Method to get an specific folder
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Returns a json object of the folder in succes
 */
exports.getFolder = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                return [4 /*yield*/, typeorm_1.getRepository(Folder_entity_1.Folder)
                        .findOne(id, { relations: ['study_set'] })
                        .then(function (folder) { return res.status(200).json(folder); })
                        .catch(function (error) { return res.status(505).json(error); })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
/**
 * Method to create a new folder
 * @param req any due to req.user
 * @param res Response
 * @param next NextFunction
 * Returns a json object of the folder created
 */
exports.postCreateFolder = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, title, description, newFolder;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                if (!req.user) return [3 /*break*/, 2];
                id = req.user.id;
                _a = req.body, title = _a.title, description = _a.description;
                newFolder = typeorm_1.getRepository(Folder_entity_1.Folder).create({ user: req.user, title: title, description: description });
                return [4 /*yield*/, typeorm_1.getRepository(Folder_entity_1.Folder)
                        .save(newFolder)
                        .then(function (folder) { return res.status(200).json(folder); })
                        .catch(function (error) { return res.status(505).json(error); })];
            case 1:
                _b.sent();
                return [3 /*break*/, 3];
            case 2:
                res.status(404).json({ message: 'Not user found' });
                _b.label = 3;
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to Edit a folder
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns a json object of the edited folder in success
 */
exports.postEditFolder = function (req, res, next) {
    var id = req.params.id;
    var _a = req.body, title = _a.title, description = _a.description;
    typeorm_1.getRepository(Folder_entity_1.Folder)
        .findOne(id, { relations: ['user'], loadRelationIds: true })
        .then(function (folderToEdit) {
        if (folderToEdit.user === req.user.id) {
            (folderToEdit.title = title), (folderToEdit.description = description);
            typeorm_1.getRepository(Folder_entity_1.Folder)
                .update(folderToEdit.id, { title: title, description: description })
                .then(function (response) { return res.status(200).json(response); })
                .catch(function (error) { return res.status(505).json(error); });
        }
        else {
            res.status(404).json({ message: 'Invalid Operation' });
        }
    })
        .catch(function (error) { return res.status(505).json(error); });
};
/**
 * Method to add to folder an study set
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns the study set added on success
 */
exports.postAddToFolder = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, study_set_id;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                study_set_id = req.body.study_set_id;
                return [4 /*yield*/, typeorm_1.getRepository(Folder_entity_1.Folder)
                        .findOne(id, { relations: ['user'], loadRelationIds: true })
                        .then(function (folderToAdd) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!(folderToAdd.user === req.user.id)) return [3 /*break*/, 2];
                                    return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                                            .findOne(study_set_id)
                                            .then(function (studySetToAdd) { return __awaiter(void 0, void 0, void 0, function () {
                                            return __generator(this, function (_a) {
                                                typeorm_1.getRepository(StudySet_entity_1.StudySet)
                                                    .update(studySetToAdd.id, { folder: folderToAdd })
                                                    .then(function (response) { return res.status(200).json(response); })
                                                    .catch(function (error) { return res.status(500).json(error); });
                                                return [2 /*return*/];
                                            });
                                        }); })
                                            .catch(function (error) { return res.status(500).json(error); })];
                                case 1:
                                    _a.sent();
                                    return [3 /*break*/, 3];
                                case 2:
                                    res.status(404).json({ message: 'Invalid Operation' });
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })
                        .catch(function (error) { return res.status(500).json(error); })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
/**
 * Method to remove a study set from folder
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns the study set removed
 */
exports.postRemoveFromFolder = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, study_set_id;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                study_set_id = req.body.study_set_id;
                return [4 /*yield*/, typeorm_1.getRepository(Folder_entity_1.Folder)
                        .findOne(id, { relations: ['user'], loadRelationIds: true })
                        .then(function (folder) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!(folder.user === req.user.id)) return [3 /*break*/, 2];
                                    return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                                            .findOne(study_set_id)
                                            .then(function (studySetToRemove) { return __awaiter(void 0, void 0, void 0, function () {
                                            return __generator(this, function (_a) {
                                                typeorm_1.getRepository(StudySet_entity_1.StudySet)
                                                    .update(studySetToRemove.id, { folder: null })
                                                    .then(function (response) { return res.status(200).json(response); })
                                                    .catch(function (error) { return res.status(500).json(error); });
                                                return [2 /*return*/];
                                            });
                                        }); })
                                            .catch(function (error) { return res.status(500).json(error); })];
                                case 1:
                                    _a.sent();
                                    return [3 /*break*/, 3];
                                case 2:
                                    res.status(404).json({ message: 'Invalid Operation' });
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })
                        .catch(function (error) { return res.status(500).json(error); })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
/**
 * Method to delete a folder
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns a message on success
 */
exports.getDeleteFolder = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                if (!req.user) return [3 /*break*/, 2];
                return [4 /*yield*/, typeorm_1.getRepository(Folder_entity_1.Folder)
                        .findOne(id, { relations: ['user'], loadRelationIds: true })
                        .then(function (folder) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!(folder.user === req.user.id)) return [3 /*break*/, 2];
                                    return [4 /*yield*/, typeorm_1.getRepository(Folder_entity_1.Folder)
                                            .delete(id)
                                            .then(function () { return res.status(200).json({ message: 'Folder deleted' }); })
                                            .catch(function (error) { return res.status(500).json(error); })];
                                case 1:
                                    _a.sent();
                                    return [3 /*break*/, 3];
                                case 2:
                                    res.status(404).json({ message: 'Invalid Operation' });
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })
                        .catch(function () { return res.status(404).json({ message: 'Invalid Operation' }); })];
            case 1:
                _a.sent();
                _a.label = 2;
            case 2: return [2 /*return*/];
        }
    });
}); };
