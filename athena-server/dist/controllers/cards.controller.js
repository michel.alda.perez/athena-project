"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDeleteCard = exports.postEditCard = exports.postCreateCard = exports.getCard = void 0;
var typeorm_1 = require("typeorm");
var StudyCard_entity_1 = require("../entities/StudyCard.entity");
var StudySet_entity_1 = require("../entities/StudySet.entity");
/**
 * Method to get an specific card
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Return a json object of the card in success
 */
exports.getCard = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                return [4 /*yield*/, typeorm_1.getRepository(StudyCard_entity_1.StudyCard)
                        .findOne(id)
                        .then(function (card) { return res.status(200).json(card); })
                        .catch(function (error) { return res.status(505).json(error); })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
/**
 * Method to create a card in a Study Set
 * @param req any due to req.user.id
 * @param res Reponse
 * @param next NextFunction
 * Returns a json object of the card created in success
 */
exports.postCreateCard = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, term, definition, card_image, study_set;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, term = _a.term, definition = _a.definition, card_image = _a.card_image, study_set = _a.study_set;
                if (!req.user) return [3 /*break*/, 2];
                return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                        .findOne(study_set, { relations: ['user'], loadRelationIds: true })
                        .then(function (studySet) { return __awaiter(void 0, void 0, void 0, function () {
                        var newCard;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!(studySet.user === req.user.id)) return [3 /*break*/, 2];
                                    newCard = typeorm_1.getRepository(StudyCard_entity_1.StudyCard).create({
                                        term: term,
                                        definition: definition,
                                        card_image: card_image,
                                        study_set: study_set,
                                    });
                                    return [4 /*yield*/, typeorm_1.getRepository(StudyCard_entity_1.StudyCard)
                                            .save(newCard)
                                            .then(function (cardCreated) { return res.status(200).json(cardCreated); })
                                            .catch(function (error) { return res.status(505).json(error); })];
                                case 1:
                                    _a.sent();
                                    return [3 /*break*/, 3];
                                case 2:
                                    res.status(404).json({ message: 'Invalid operation' });
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })
                        .catch(function () { return res.status(404).json({ message: 'Invalid operation' }); })];
            case 1:
                _b.sent();
                return [3 /*break*/, 3];
            case 2:
                res.status(404).json({ message: 'Not User Found, Invalid operation' });
                _b.label = 3;
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to edit a card
 * @param req Request
 * @param res Response
 * @param next Nextfunction
 * Returns the edited card in success
 */
exports.postEditCard = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, term, definition, status, card_image;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                id = req.params.id;
                _a = req.body, term = _a.term, definition = _a.definition, status = _a.status, card_image = _a.card_image;
                if (!req.user) return [3 /*break*/, 2];
                return [4 /*yield*/, typeorm_1.getRepository(StudyCard_entity_1.StudyCard)
                        .findOne(id, { relations: ['study_set'], loadRelationIds: true })
                        .then(function (cardToEdit) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, validateUserFromCard(id, req).then(function (isValid) { return __awaiter(void 0, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    if (!isValid) return [3 /*break*/, 2];
                                                    cardToEdit.term = term;
                                                    cardToEdit.definition = definition;
                                                    cardToEdit.status = status;
                                                    cardToEdit.card_image = card_image;
                                                    return [4 /*yield*/, typeorm_1.getRepository(StudyCard_entity_1.StudyCard)
                                                            .save(cardToEdit)
                                                            .then(function (cardEdited) { return res.status(200).json(cardEdited); })
                                                            .catch(function (error) { return res.status(505).json(error); })];
                                                case 1:
                                                    _a.sent();
                                                    return [3 /*break*/, 3];
                                                case 2:
                                                    res.status(404).json({ message: 'Invalid Operation' });
                                                    _a.label = 3;
                                                case 3: return [2 /*return*/];
                                            }
                                        });
                                    }); })];
                                case 1:
                                    _a.sent();
                                    return [2 /*return*/];
                            }
                        });
                    }); })];
            case 1:
                _b.sent();
                _b.label = 2;
            case 2: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to delete a card
 * @param req Request
 * @param res Response
 * @param next Nextfunction
 */
exports.getDeleteCard = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                if (!req.user) return [3 /*break*/, 2];
                return [4 /*yield*/, validateUserFromCard(id, req).then(function (isValid) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!isValid) return [3 /*break*/, 2];
                                    return [4 /*yield*/, typeorm_1.getRepository(StudyCard_entity_1.StudyCard)
                                            .delete(id)
                                            .then(function () { return res.status(200).json({ message: 'Card deleted' }); })
                                            .catch(function (error) { return res.status(505).json(error); })];
                                case 1:
                                    _a.sent();
                                    return [3 /*break*/, 3];
                                case 2:
                                    res.status(404).json({ message: 'Invalid operation' });
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })];
            case 1:
                _a.sent();
                _a.label = 2;
            case 2: return [2 /*return*/];
        }
    });
}); };
/**
 * Method to check if the user loggedin is the owner to allow delet card
 * @param cardId Card Id to delete
 * @param req any due to req.user
 * Returns a promise<Boolean> where true if the user is the owner
 */
var validateUserFromCard = function (cardId, req) { return __awaiter(void 0, void 0, void 0, function () {
    var isValid;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, typeorm_1.getRepository(StudyCard_entity_1.StudyCard)
                    .findOne(cardId, {
                    relations: ['study_set'],
                    loadRelationIds: true,
                })
                    .then(function (card) { return __awaiter(void 0, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, typeorm_1.getRepository(StudySet_entity_1.StudySet)
                                    .findOne(card.study_set, {
                                    relations: ['user'],
                                    loadRelationIds: true,
                                })
                                    .then(function (set) {
                                    return set.user === req.user.id && card !== undefined
                                        ? (isValid = true)
                                        : (isValid = false);
                                })
                                    .catch(function () { return (isValid = false); })];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                }); })
                    .catch(function () { return (isValid = false); })];
            case 1:
                _a.sent();
                return [2 /*return*/, isValid];
        }
    });
}); };
