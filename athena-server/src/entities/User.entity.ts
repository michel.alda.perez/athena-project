import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, Index } from 'typeorm';
import { Folder } from './Folder.entity';
import { StudySet } from './StudySet.entity';

@Entity()
@Index('email_user', ['email'])
@Index('username_user', ['username'])
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  username: string;
  @Column({ nullable: true })
  full_name: string;
  @Column()
  email: string;
  @Column({ nullable: true })
  password: string;
  @Column({
    default:
      'https://res.cloudinary.com/dbc1hdxqi/image/upload/v1595354981/athena-project/assets/athena-default-icon_f4swin.png',
  })
  profile_photo: string;
  @Column({ default: false })
  username_changed: boolean;
  @Column({ nullable: true })
  google_id: string;
  @Column({ nullable: true })
  facebook_id: string;
  /* User contains multiples instances of folder, but folder only one instance of User */
  @OneToMany((type) => Folder, (folder) => folder.user)
  folders: Folder[];
  /* User contains multiples instances of study set, but study set only one instance of User */
  @OneToMany((type) => StudySet, (study_set) => study_set.user)
  study_sets: StudySet[];
}
