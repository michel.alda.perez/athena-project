import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { StudySet } from './StudySet.entity';

@Entity()
export class StudyCard {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  term: string;
  @Column()
  definition: string;
  @Column({ default: 'untrained' })
  status: string;
  @Column({ default: new Date() })
  creation_date: Date;
  @Column({ nullable: true })
  card_image: Date;
  /* StudySet contains multiples instances of card, but card only one instance of StudySet */
  @ManyToOne((type) => StudySet, (study_set) => study_set.study_cards, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  study_set: StudySet;
}
