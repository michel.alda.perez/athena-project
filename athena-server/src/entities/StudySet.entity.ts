import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';
import { StudyCard } from './StudyCard.entity';
import { Folder } from './Folder.entity';
import { User } from './User.entity';

@Entity()
export class StudySet {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  title: string;
  @Column()
  description: string;
  @Column({ default: 'public' })
  visibility: string;
  @Column()
  creation_date: Date;
  @Column({ nullable: true })
  completation_date: Date;
  @Column({ default: 0.0 })
  grade: number;
  @Column()
  original_user_id: string;
  @ManyToOne((type) => User, (user) => user.study_sets, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  user: User;
  @ManyToOne((type) => Folder, (folder) => folder.study_set, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  folder: Folder;
  /* StudySet contains multiples instances of card, but card only one instance of StudySet */
  @OneToMany((type) => StudyCard, (study_card) => study_card.study_set)
  study_cards: StudyCard[];
}
