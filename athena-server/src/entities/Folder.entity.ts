import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { User } from './User.entity';
import { StudySet } from './StudySet.entity';

@Entity()
export class Folder {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  title: string;
  @Column()
  description: string;
  @Column({ default: new Date() })
  creation_date: Date;
  @ManyToOne((type) => User, (user) => user.folders, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  user: User;
  /* Folder contains multiples instances of study sets, but study sets only one instance of Folder */
  @OneToMany((type) => StudySet, (study_set) => study_set.folder)
  study_set: StudySet[];
}
