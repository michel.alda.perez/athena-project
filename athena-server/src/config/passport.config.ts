import passport from 'passport';
import bcrypt from 'bcrypt';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as GoogleStrategy } from 'passport-google-oauth20';
import { Strategy as FacebookStrategy } from 'passport-facebook';
import { getRepository } from 'typeorm';
import { User } from '../entities/User.entity';

const verifyCallback = async (username, password, done) => {
  try {
    const user = await getRepository(User).findOne({ username });
    if (!user) {
      return done(null, false, { invalidCredentials: true });
    }
    if (!user.password) {
      return done(null, false, {
        message:
          'You signup using one of your social accounts, register your email or login using a social account',
      });
    }
    const isValid = bcrypt.compareSync(password, user.password);
    return isValid ? done(null, user) : done(null, false, { invalidCredentials: true });
  } catch (err) {
    done('There was an error with the server', false, {
      message: 'Server internal error',
    });
  }
};

const strategy = new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password',
  },
  verifyCallback
);

const googleVerifyCallback = async (_, __, profile, done) => {
  const user: User = await getRepository(User)
    .findOne({ google_id: profile.id })
    .catch((error) => done(error));
  if (user) {
    user.profile_photo = profile._json.picture;
    await getRepository(User).save(user);
    done(null, user);
  } else {
    const newUser = getRepository(User).create({
      google_id: profile.id,
      username: profile.displayName,
      full_name: profile.displayName,
      email: profile._json.email,
      profile_photo: profile._json.picture,
    });
    await getRepository(User).save(newUser);
    done(null, newUser);
  }
};

const googleStrategy = new GoogleStrategy(
  {
    clientID: process.env.GOOGLE_ID,
    clientSecret: process.env.GOOGLE_SECRET,
    callbackURL: `/auth/google/callback`,
  },
  googleVerifyCallback
);

const facebookVerifyCallback = async (_, __, profile, done) => {
  const user: User = await getRepository(User)
    .findOne({ facebook_id: profile.id })
    .catch((error) => done(error));
  if (user) {
    user.profile_photo = profile.photos[0].value;
    await getRepository(User).save(user);
    done(null, user);
  } else {
    const newUser = getRepository(User).create({
      facebook_id: profile.id,
      username: profile.displayName,
      full_name: profile.displayName,
      email: profile.emails[0].value,
      profile_photo: profile.profile_pic,
    });
    await getRepository(User).save(newUser);
    done(null, newUser);
  }
};

const facebookStrategy = new FacebookStrategy(
  {
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET,
    callbackURL: `/auth/facebook/callback`,
    profileFields: ['id', 'emails', 'photos', 'displayName', 'profileUrl', 'gender'],
  },
  facebookVerifyCallback
);

passport.use(strategy);
passport.use(googleStrategy);
passport.use(facebookStrategy);

passport.serializeUser((user: User, cb) => {
  cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
  getRepository(User)
    .findOne(id)
    .then((user) => cb(null, user))
    .catch((error) => cb(error, null));
});

export default passport;
