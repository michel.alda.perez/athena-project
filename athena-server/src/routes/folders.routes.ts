import { Router } from 'express';
import {
  getFolders,
  getFolder,
  postCreateFolder,
  postEditFolder,
  postAddToFolder,
  postRemoveFromFolder,
  getDeleteFolder,
} from '../controllers/folders.controller';

const router = Router();

// Get all the folders from user
router.get('/folders/', getFolders);
// Get specific folder from user folders
router.get('/folders/:id', getFolder);
// Create a new folder
router.post('/folders/create', postCreateFolder);
// Edit a folder
router.post('/folders/:id/edit', postEditFolder);
// Add study set to specific folder
router.post('/folders/:id/add', postAddToFolder);
// Remove study set to specific folder
router.post('/folders/:id/remove', postRemoveFromFolder);
// Delete a folder
router.get('/folders/:id/delete', getDeleteFolder);

export default router;
