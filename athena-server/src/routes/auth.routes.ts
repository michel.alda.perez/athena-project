import { Router } from 'express';
import passport from '../config/passport.config';
import {
  postUpload,
  postSignup,
  postEdit,
  postLogin,
  logout,
  loggedin,
  verifyEmail,
  verifyUsername,
  getDelete,
  getUser,
  getUserPhoto,
} from '../controllers/auth.controller';
import uploadCloud from '../config/cloudinary.config';

const router = Router();

// Check if user is logged in
router.get('/loggedin', loggedin);
// Get User Info
router.get('/user', getUser);
// Get User Photo
router.get('/user/:id', getUserPhoto);
// Sing up an user
router.post('/signup', postSignup);
// Log in an user
router.post('/login', passport.authenticate('local'), postLogin);
// Edit user
router.post('/edit', postEdit);
// Upload profile photo
router.post('/upload', uploadCloud.single('profile_photo'), postUpload);
// Check if email is available to use
router.post('/available/email', verifyEmail);
// Check if username is available to use
router.post('/available/username', verifyUsername);
// Log out an user
router.get('/logout', logout);
// Delete an user
router.get('/delete', getDelete);

// Google Authentication
router.get(
  '/google',
  passport.authenticate('google', {
    scope: ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email'],
  })
);

router.get(
  '/google/callback',
  passport.authenticate('google', {
    failureRedirect: `${process.env.ENDPOINT}`,
  }),
  (req, res) => {
    res.redirect(`${process.env.ENDPOINT}/home?authenticated=true`);
  }
);

// Facebook Authentication
router.get(
  '/facebook',
  passport.authenticate('facebook', {
    scope: ['email'],
  })
);

router.get(
  '/facebook/callback',
  passport.authenticate('facebook', {
    failureRedirect: `${process.env.ENDPOINT}`,
  }),
  (req, res) => {
    res.redirect(`${process.env.ENDPOINT}/home?authenticated=true`);
  }
);

export default router;
