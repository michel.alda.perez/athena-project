import { Router } from 'express';
import {
  getCard,
  postCreateCard,
  postEditCard,
  getDeleteCard,
} from '../controllers/cards.controller';

const router = Router();

// Get specific card
router.get('/study_cards/:id', getCard);
// Create a new card
router.post('/study_cards/create', postCreateCard);
// Edit specific card
router.post('/study_cards/:id/edit', postEditCard);
// Delete specific card
router.get('/study_cards/:id/delete', getDeleteCard);

export default router;
