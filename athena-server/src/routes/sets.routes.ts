import { Router } from 'express';
import {
  getStudySets,
  getStudySet,
  getCreateStudySet,
  getSaveStudySet,
  getDeleteStudySet,
  postEditStudySet,
  getUserStudySets,
} from '../controllers/sets.controller';

const router = Router();

// Get all public study sets
router.get('/study_sets', getStudySets);
// Get all study sets of user
router.get('/study_sets/user', getUserStudySets);
// Get specific study set
router.get('/study_sets/:id', getStudySet);
// Create a new study set
router.post('/study_sets/create', getCreateStudySet);
// Save a study set from other user
router.get('/study_sets/:id/save', getSaveStudySet);
// Edit an study set
router.post('/study_sets/:id/edit', postEditStudySet);
// Delete an study set
router.get('/study_sets/:id/delete', getDeleteStudySet);

export default router;
