#!/usr/bin/env node
require('dotenv').config();

import 'reflect-metadata';
import express from 'express';
import logger from 'morgan';
import path from 'path';
import cors from 'cors';
import ExpressSession from 'express-session';
import { createConnection } from 'typeorm';
import { TypeormStore } from 'connect-typeorm/out';

import { Session } from './entities/Session.entity';
import passport from './config/passport.config';

import authRoutes from './routes/auth.routes';
import setsRoutes from './routes/sets.routes';
import cardsRoutes from './routes/cards.routes';
import foldersRoutes from './routes/folders.routes';

async function bootstrap() {
  const app_name = require('../package.json').name;
  const debug = require('debug')(`${app_name}:${path.basename(__filename).split('.')[0]}`);

  const database_url =
    process.env.ENV === 'development'
      ? `postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@localhost:5432/${process.env.DB}`
      : process.env.DATABASE_URL;

  //DB connection
  const connection = await createConnection({
    type: 'postgres',
    url: database_url,
    entities: [path.join(__dirname, './entities/**/*.{ts,js}')],
    migrations: [path.join(__dirname, './migrations/**/*.{ts,js}')],
    cli: {
      migrationsDir: 'migrations',
    },
    synchronize: true,
  });
  // display DB info
  console.log('connected to PostgreSQL! Database options: ', connection.options);

  // setup an express app
  const app = express();

  // Middleware Setup
  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  // Configure express-session
  app.use(
    ExpressSession({
      secret: process.env.SECRET,
      resave: true,
      saveUninitialized: true,
      cookie: { maxAge: 1000 * 60 * 60 * 24 },
      store: new TypeormStore({
        cleanupLimit: 2,
        limitSubquery: false,
        ttl: 86400,
      }).connect(connection.getRepository(Session)),
    })
  );

  // passport
  app.use(passport.initialize());
  app.use(passport.session());

  // default value for title local
  app.locals.title = 'Express - Generated with kavak typescript generator for Athena Project';

  // config of middleware cors
  app.use(
    cors({
      credentials: true,
      origin: [process.env.ENDPOINT],
    })
  );

  // app routes
  app.use('/auth', authRoutes);
  app.use('/api', setsRoutes);
  app.use('/api', cardsRoutes);
  app.use('/api', foldersRoutes);
  app.use('/', (req, res) => {
    res.status(200).json({ message: 'Athena App Server Working 👌' });
  });

  return app;
}

module.exports = bootstrap;
