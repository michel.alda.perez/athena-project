import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { StudyCard } from '../entities/StudyCard.entity';
import { StudySet } from '../entities/StudySet.entity';

/**
 * Method to get an specific card
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Return a json object of the card in success
 */
export const getCard = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  await getRepository(StudyCard)
    .findOne(id)
    .then((card) => res.status(200).json(card))
    .catch((error) => res.status(505).json(error));
};

/**
 * Method to create a card in a Study Set
 * @param req any due to req.user.id
 * @param res Reponse
 * @param next NextFunction
 * Returns a json object of the card created in success
 */
export const postCreateCard = async (req: any, res: Response, next: NextFunction) => {
  const { term, definition, card_image, study_set } = req.body;
  if (req.user) {
    await getRepository(StudySet)
      .findOne(study_set, { relations: ['user'], loadRelationIds: true })
      .then(async (studySet) => {
        if (studySet.user === req.user.id) {
          const newCard = getRepository(StudyCard).create({
            term,
            definition,
            card_image,
            study_set,
          });
          await getRepository(StudyCard)
            .save(newCard)
            .then((cardCreated) => res.status(200).json(cardCreated))
            .catch((error) => res.status(505).json(error));
        } else {
          res.status(404).json({ message: 'Invalid operation' });
        }
      })
      .catch(() => res.status(404).json({ message: 'Invalid operation' }));
  } else {
    res.status(404).json({ message: 'Not User Found, Invalid operation' });
  }
};

/**
 * Method to edit a card
 * @param req Request
 * @param res Response
 * @param next Nextfunction
 * Returns the edited card in success
 */
export const postEditCard = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const { term, definition, status, card_image } = req.body;
  if (req.user) {
    await getRepository(StudyCard)
      .findOne(id, { relations: ['study_set'], loadRelationIds: true })
      .then(async (cardToEdit) => {
        await validateUserFromCard(id, req).then(async (isValid) => {
          if (isValid) {
            cardToEdit.term = term;
            cardToEdit.definition = definition;
            cardToEdit.status = status;
            cardToEdit.card_image = card_image;
            await getRepository(StudyCard)
              .save(cardToEdit)
              .then((cardEdited) => res.status(200).json(cardEdited))
              .catch((error) => res.status(505).json(error));
          } else {
            res.status(404).json({ message: 'Invalid Operation' });
          }
        });
      });
  }
};

/**
 * Method to delete a card
 * @param req Request
 * @param res Response
 * @param next Nextfunction
 */
export const getDeleteCard = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  if (req.user) {
    await validateUserFromCard(id, req).then(async (isValid) => {
      if (isValid) {
        await getRepository(StudyCard)
          .delete(id)
          .then(() => res.status(200).json({ message: 'Card deleted' }))
          .catch((error) => res.status(505).json(error));
      } else {
        res.status(404).json({ message: 'Invalid operation' });
      }
    });
  }
};

/**
 * Method to check if the user loggedin is the owner to allow delet card
 * @param cardId Card Id to delete
 * @param req any due to req.user
 * Returns a promise<Boolean> where true if the user is the owner
 */
const validateUserFromCard = async (cardId: string, req: any): Promise<Boolean> => {
  let isValid: boolean;
  await getRepository(StudyCard)
    .findOne(cardId, {
      relations: ['study_set'],
      loadRelationIds: true,
    })
    .then(async (card) => {
      await getRepository(StudySet)
        .findOne(card.study_set, {
          relations: ['user'],
          loadRelationIds: true,
        })
        .then((set) =>
          set.user === req.user.id && card !== undefined
            ? (isValid = true)
            : (isValid = false)
        )
        .catch(() => (isValid = false));
    })
    .catch(() => (isValid = false));
  return isValid;
};
