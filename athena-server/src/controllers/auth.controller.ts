import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import bcrypt from 'bcrypt';
import { User } from '../entities/User.entity';

/**
 * Method to retrieve an user information
 * @param req Request (any) due to req.user.id
 * @param res Response
 * @param next Next Function
 * Returns a json object with the user info on success
 * a message 'Not User Found' if not user was founded
 * otherwise an error with status 500u
 */
export const getUser = async (req: any, res: Response, next: NextFunction) => {
  try {
    if (req.user) {
      const { id } = req.user;
      console.log('getuser id: ', id);
      const user = await getRepository(User).findOne({ id });
      return res.status(200).json(user);
    } else {
      return res.status(404).json({ message: 'Not User Found' });
    }
  } catch (error) {
    res.status(500).json(error);
  }
};

export const getUserPhoto = async (req: any, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    console.log('getuser photo id: ', id);
    const user = await getRepository(User).findOne({ id });
    return res.status(200).json({ profile_photo: user.profile_photo });
  } catch (error) {
    return res.status(500).json(error);
  }
};

/**
 * Method to sign up a new user, if email doesn't exist
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Returns the user and authenticated on success
 * An error 409 (conflic) if the user exists
 * An internal server error otherwise
 */
export const postSignup = (req: Request, res: Response, next: NextFunction) => {
  const { username, email, password } = req.body;
  getRepository(User)
    .findOne({ email })
    .then((userFounded) => {
      if (!userFounded) {
        const hashPassword = generatePassword(password);
        const newUser = getRepository(User).create({
          email,
          username,
          password: hashPassword,
        });
        getRepository(User)
          .save(newUser)
          .then((user) => {
            req.login(user, (err) => {
              if (err) return next(err);
            });
            return res.status(200).json({ user, authenticated: true });
          })
          .catch((error) => {
            return res.status(500).json({ message: 'Internal Server Error' });
          });
      } else {
        return res.status(409).json({ message: 'The email is already in use', authenticated: false });
      }
    })
    .catch((error) => {
      return res.status(500).json({ message: 'Internal Server Error' });
    });
};

/**
 * Method to log in a user with passport (local)
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Returns the user and authenticated on success
 * Otherwise an error and invalidCredentials true
 */
export const postLogin = async (req: Request, res: Response, next: NextFunction) => {
  try {
    res.status(200).send({ user: req.user, authenticated: true, invalidCredentials: false });
  } catch (error) {
    res.status(409).json({ invalidCredentials: true, authenticated: false });
  }
};

/**
 * Method to log out an user, clearing the cookie and set authenticated false
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
export const logout = (req: Request, res: Response, next: NextFunction) => {
  try {
    req.logOut();
    res.status(200).clearCookie('connect.sid', {});
    req.session.destroy(() => {
      res.json({ authenticated: false });
    });
  } catch (error) {
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};

/**
 * Method that uses cloudinary and multer to upload profile photo
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns a json object with the user updated on success
 * Otherwise an 'Internal Server Error' with status 500
 */
export const postUpload = async (req: any, res: Response, next: NextFunction) => {
  try {
    const { profile_photo } = req.body;
    const { id } = req.user;
    let userToUpdate = {};
    userToUpdate['profile_photo'] = req.file ? req.file.path : profile_photo;
    const user = await getRepository(User).update(id, {
      profile_photo: userToUpdate['profile_photo'],
    });
    return user ? res.status(200).json(user) : res.status(500).json({ message: 'Internal Server Error' });
  } catch (error) {
    res.status(500).json(error);
  }
};

/**
 * Method to edit the user info, username can only be changed once
 * @param req Request any due to req.user.id
 * @param res Response
 * @param next NextFunction
 */
export const postEdit = async (req: any, res: Response, next: NextFunction) => {
  try {
    const { id } = req.user;
    const { username, email, password, full_name } = req.body;
    const userToEdit = await getRepository(User).findOne(id);
    if (userToEdit) {
      if (password !== '') {
        const isMatch = bcrypt.compareSync(password, userToEdit.password);
        if (!isMatch) userToEdit.password = generatePassword(password);
      }
      if (userToEdit.username_changed === false) {
        if (username !== '' && username !== userToEdit.username) {
          userToEdit.username_changed = true;
          userToEdit.username = username;
        }
      }
      userToEdit.email = email;
      userToEdit.full_name = full_name;
      try {
        const user = await getRepository(User).save(userToEdit);
        return user ? res.status(200).json(user) : res.status(500).json({ message: 'internal Server Error' });
      } catch (error) {
        return res.status(500).json({ message: 'internal Server Error' });
      }
    }
  } catch (error) {
    return res.status(500).json({ message: 'internal Server Error' });
  }
};

/**
 * Method to verify if user is logged in the website
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns authenticated true and the user object if true otherwise authenticated false
 */
export const loggedin = async (req: any, res: Response, next: NextFunction) => {
  try {
    if (req.user) {
      const { id } = req.user;
      await getRepository(User).findOne(id);
      return res.status(200).json({ authenticated: true });
    } else {
      return res.status(200).json({ authenticated: false });
    }
  } catch (error) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

/**
 * Method to delete an user
 * @param req any due to req.user
 * @param res Response
 * @param next NextFunction
 * Returns authenticated false and response on delete
 */
export const getDelete = async (req: any, res: Response, next: NextFunction) => {
  try {
    if (req.user) {
      const { id } = req.user;
      const userFounded = await getRepository(User).findOne(id);
      if (userFounded) {
        req.logOut();
        res.clearCookie('connect.sid', {});
        req.session.destroy(async () => {
          try {
            await getRepository(User).delete(userFounded);
            return res.status(200).json({ authenticated: false });
          } catch (error) {
            return res.status(500).json(error);
          }
        });
      } else {
        return res.status(404).json({ message: 'Not User Found' });
      }
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

/**
 * Verify if email is already in the DB
 * @param req Any due to req.user.email
 * @param res Response
 * @param next NextFunction
 * Returns available that check if the email has been register in the database
 * If an user is loggedin returns true if the actual email is the same
 *  to avoid unique email validator in edit form
 * Otherwise returns true or false if email is or not available
 */
export const verifyEmail = async (req: any, res: Response, next: NextFunction) => {
  try {
    const { email } = req.body;
    if (req.user) {
      const actualEmail = req.user.email;
      if (email === actualEmail) {
        return res.status(200).json({ available: true });
      }
    }
    const user = await getRepository(User).findOne({ email });
    !user ? res.status(200).json({ available: true }) : res.status(200).json({ available: false });
  } catch (error) {
    res.status(500).json(error);
  }
};

/**
 * Method to verify is username is already in the DB
 * @param req any due to req.user.username
 * @param res Response
 * @param next NextFunction
 * Returns available that check if the username has been register in the database
 * If an user is loggedin returns true if the actual username is the same
 *  to avoid unique email validator in edit form
 * Otherwise returns true or false if username is or not available
 */
export const verifyUsername = async (req: any, res: Response, next: NextFunction) => {
  try {
    const { username } = req.body;
    if (req.user) {
      const actualUsername = req.user.username;
      if (username === actualUsername) {
        return res.status(200).json({ available: true });
      }
    }
    const user = await getRepository(User).findOne({ username });
    !user ? res.status(200).json({ available: true }) : res.status(200).json({ available: false });
  } catch (error) {
    res.status(500).json(error);
  }
};

// Auxiliar Functions

/**
 * Method to generate a hashed password
 * @param password string password
 * Returns a hashed password
 */
const generatePassword = (password: string): string => {
  const salt = bcrypt.genSaltSync(10);
  const hashPassword = bcrypt.hashSync(password, salt);
  return hashPassword;
};
