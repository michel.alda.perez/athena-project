import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { Folder } from '../entities/Folder.entity';
import { User } from '../entities/User.entity';
import { StudySet } from '../entities/StudySet.entity';

/**
 * Method to get all folders from user
 * @param req any due to req.user
 * @param res Response
 * @param next NextFucntion
 * Returns an json object array of user folders in success
 */
export const getFolders = (req: any, res: Response, next: NextFunction) => {
  const { id } = req.user;
  getRepository(User)
    .findOne(id, { relations: ['folders'] })
    .then((data) => res.status(200).json(data.folders))
    .catch((error) => res.status(505).json(error));
};

/**
 * Method to get an specific folder
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Returns a json object of the folder in succes
 */
export const getFolder = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  await getRepository(Folder)
    .findOne(id, { relations: ['study_set'] })
    .then((folder) => res.status(200).json(folder))
    .catch((error) => res.status(505).json(error));
};

/**
 * Method to create a new folder
 * @param req any due to req.user
 * @param res Response
 * @param next NextFunction
 * Returns a json object of the folder created
 */
export const postCreateFolder = async (req: any, res: Response, next: NextFunction) => {
  if (req.user) {
    const { id } = req.user;
    const { title, description } = req.body;
    const newFolder = getRepository(Folder).create({ user: req.user, title, description });
    await getRepository(Folder)
      .save(newFolder)
      .then((folder) => res.status(200).json(folder))
      .catch((error) => res.status(505).json(error));
  } else {
    res.status(404).json({ message: 'Not user found' });
  }
};

/**
 * Method to Edit a folder
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns a json object of the edited folder in success
 */
export const postEditFolder = (req: any, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const { title, description } = req.body;
  getRepository(Folder)
    .findOne(id, { relations: ['user'], loadRelationIds: true })
    .then((folderToEdit) => {
      if (folderToEdit.user === req.user.id) {
        (folderToEdit.title = title), (folderToEdit.description = description);
        getRepository(Folder)
          .update(folderToEdit.id, { title: title, description: description })
          .then((response) => res.status(200).json(response))
          .catch((error) => res.status(505).json(error));
      } else {
        res.status(404).json({ message: 'Invalid Operation' });
      }
    })
    .catch((error) => res.status(505).json(error));
};

/**
 * Method to add to folder an study set
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns the study set added on success
 */
export const postAddToFolder = async (req: any, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const { study_set_id } = req.body;
  await getRepository(Folder)
    .findOne(id, { relations: ['user'], loadRelationIds: true })
    .then(async (folderToAdd) => {
      if (folderToAdd.user === req.user.id) {
        await getRepository(StudySet)
          .findOne(study_set_id)
          .then(async (studySetToAdd) => {
            getRepository(StudySet)
              .update(studySetToAdd.id, { folder: folderToAdd })
              .then((response) => res.status(200).json(response))
              .catch((error) => res.status(500).json(error));
          })
          .catch((error) => res.status(500).json(error));
      } else {
        res.status(404).json({ message: 'Invalid Operation' });
      }
    })
    .catch((error) => res.status(500).json(error));
};

/**
 * Method to remove a study set from folder
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns the study set removed
 */
export const postRemoveFromFolder = async (req: any, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const { study_set_id } = req.body;
  await getRepository(Folder)
    .findOne(id, { relations: ['user'], loadRelationIds: true })
    .then(async (folder) => {
      if (folder.user === req.user.id) {
        await getRepository(StudySet)
          .findOne(study_set_id)
          .then(async (studySetToRemove) => {
            getRepository(StudySet)
              .update(studySetToRemove.id, { folder: null })
              .then((response) => res.status(200).json(response))
              .catch((error) => res.status(500).json(error));
          })
          .catch((error) => res.status(500).json(error));
      } else {
        res.status(404).json({ message: 'Invalid Operation' });
      }
    })
    .catch((error) => res.status(500).json(error));
};

/**
 * Method to delete a folder
 * @param req any due to req.user.id
 * @param res Response
 * @param next NextFunction
 * Returns a message on success
 */
export const getDeleteFolder = async (req: any, res: Response, next: NextFunction) => {
  const { id } = req.params;
  if (req.user) {
    await getRepository(Folder)
      .findOne(id, { relations: ['user'], loadRelationIds: true })
      .then(async (folder) => {
        if (folder.user === req.user.id) {
          await getRepository(Folder)
            .delete(id)
            .then(() => res.status(200).json({ message: 'Folder deleted' }))
            .catch((error) => res.status(500).json(error));
        } else {
          res.status(404).json({ message: 'Invalid Operation' });
        }
      })
      .catch(() => res.status(404).json({ message: 'Invalid Operation' }));
  }
};
