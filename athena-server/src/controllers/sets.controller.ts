import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { StudySet } from '../entities/StudySet.entity';
import { User } from '../entities/User.entity';

/**
 * Method to get all public study sets
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Return a json object than contains an array of study sets
 */
export const getStudySets = async (req: Request, res: Response, next: NextFunction) => {
  await getRepository(StudySet)
    .find({
      where: { visibility: 'public' },
      loadRelationIds: true,
      order: { title: 'DESC', original_user_id: 'DESC' },
    })
    .then((study_sets) => res.status(200).json(study_sets))
    .catch((error) => res.status(404).json(error));
};

/**
 * Method to get only the study sets of the user
 * @param req any due to req.user
 * @param res Response
 * @param next NextFunction
 * Return a json object than contains an array of study sets of one user
 */
export const getUserStudySets = async (req: any, res: Response, next: NextFunction) => {
  if (req.user) {
    const { id } = req.user;
    getRepository(User)
      .findOne(id, { relations: ['study_sets'],  })
      .then((data) => res.status(200).json(data.study_sets))
      .catch((error) => res.status(505).json(error));
  } else {
    res.status(404).json({ message: 'Not user found' });
  }
};

/**
 * Method to get a specific study set
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Return a json object of a Study Set
 */
export const getStudySet = (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  getRepository(StudySet)
    .findOne(id, { relations: ['study_cards', 'user', 'folder'],  })
    .then((study_set) => res.status(200).json(study_set))
    .catch((error) => res.status(404).json(error));
};

/**
 * Method to create a Study Set
 * @param req any due to user id is needed
 * @param res Response
 * @param next NextFunction
 * Return a json object than contains the study set created
 */
export const getCreateStudySet = async (req: any, res: Response, next: NextFunction) => {
  if (req.user) {
    const { id } = req.user;
    const { title, description, visibility } = req.body;
    const newStudySet = getRepository(StudySet).create({
      title,
      description,
      visibility,
      creation_date: new Date(),
      user: id,
      original_user_id: id,
    });
    await getRepository(StudySet)
      .save(newStudySet)
      .then((study_set) => res.status(200).json(study_set))
      .catch((error) => res.status(505).json(error));
  }
};

/**
 * Method to save a study set from other user
 * @param req any due to user id is needed
 * @param res Response
 * @param next NextFunction
 * Return a json object than contains the study set saved
 */
export const getSaveStudySet = async (req: any, res: Response, next: NextFunction) => {
  if (req.user) {
    const { id } = req.user;
    await getRepository(StudySet)
      .findOne(req.params.id, { relations: ['study_cards', 'user'] })
      .then(async (studySetToSave) => {
        await getRepository(User)
          .findOne(studySetToSave.user.id)
          .then(async (originalUser) => {
            const studySetSaved = getRepository(StudySet).create({
              title: studySetToSave.title,
              description: studySetToSave.description,
              creation_date: studySetToSave.creation_date,
              study_cards: studySetToSave.study_cards,
              original_user_id: originalUser.id,
              user: id,
              visibility: 'saved',
            });
            await getRepository(StudySet)
              .save(studySetSaved)
              .then((study_set) => res.status(200).json(study_set))
              .catch((error) => res.status(505).json(error));
          })
          .catch((error) => res.status(505).json(error));
      })
      .catch((error) => res.status(505).json(error));
  }
};

/**
 * Method to edit a study set
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Return a json object than contains the edited study set
 */
export const postEditStudySet = async (req: any, res: Response, next: NextFunction) => {
  const { title, description, visibility } = req.body;
  const { id } = req.params;
  await getRepository(StudySet)
    .findOne(id, { relations: ['user'], loadRelationIds: true })
    .then(async (editStudySet) => {
      if (req.user.id === editStudySet.user) {
        await getRepository(StudySet).update(id, {
          title,
          description,
          visibility,
        });
        await getRepository(StudySet)
          .findOne(id, { loadRelationIds: true })
          .then((study_set) => res.status(200).json(study_set))
          .catch((error) => res.status(505).json(error));
      } else {
        res.status(404).json({ message: 'Invalid action' });
      }
    });
};

/**
 * Method to delete a study set
 * @param req Request
 * @param res Response
 * @param next NextFunction
 * Returns a message on success or error on failure
 */
export const getDeleteStudySet = async (req: any, res: Response, next: NextFunction) => {
  const { id } = req.params;
  if (req.user) {
    await getRepository(StudySet)
      .findOne(id, {
        relations: ['user'],
        loadRelationIds: true,
      })
      .then(async (studySetToDelete) => {
        if ((req.user.id = studySetToDelete.user)) {
          await getRepository(StudySet)
            .delete(id)
            .then(() => res.status(200).json({ message: 'Study Set deleted' }))
            .catch((error) => res.status(505).json(error));
        } else {
          res.status(404).json({ message: 'Invalid action' });
        }
      })
      .catch(() => res.status(404).json({ message: 'Invalid action' }));
  }
};
