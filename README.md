# Athena Project

Final bootcamp project

## Introduction

> Athena Project is a web application to use flashcards and help when studying, It allows you to create your own study sets and add your cards, also create folders to organize better your study sets as you prefer.

## Athena WebPage OverView

![Athena App Landing Page](https://res.cloudinary.com/dbc1hdxqi/image/upload/v1596214619/athena-project/assets/LandingPage_dzi1aj.png)

## Test Users

1. **username:** drHouse **password:** i<3DrCuddy **email:** drhouse@test.com
2. **username:** stevejobs **password:** wozniakFan **email:** stevejobs@test.com
3. **username:** markzuckerberg **pasword:** instabookapp **email:** markzuckerberg@test.com
4. **username:** stephenhawking **password:** sheldoncooper **email:** stephenhawking@test.com

## Diagrams of the app

### Entity Relation Diagram

![Athena App Entity Relation Diagram](https://res.cloudinary.com/dbc1hdxqi/image/upload/v1596217292/athena-project/assets/AthenaAppProject-ERD_p6b2um.png)

### FlowChart Diagram to Authentication

![Athena App FlowChart Diagram to Authentication](https://res.cloudinary.com/dbc1hdxqi/image/upload/v1596217244/athena-project/assets/AthenaAppProject-Authentication_kz2y59.png)

### FlowChart Diagram to Study Sets

![Athena App FlowChart Diagram to Study Sets](https://res.cloudinary.com/dbc1hdxqi/image/upload/v1596217181/athena-project/assets/AthenaAppProject-StudySets_ifdzcl.png)

### FlowChart Diagram to Folders

![Athena App FlowChart Diagram to Folders](https://res.cloudinary.com/dbc1hdxqi/image/upload/v1596215583/athena-project/assets/AthenaAppProject-Folders_q56gy8.png)

### FlowChart Diagram to Study Cards

![Athena App FlowChart Diagram to Study Cards](https://res.cloudinary.com/dbc1hdxqi/image/upload/v1596215660/athena-project/assets/AthenaAppProject-StudyCards_seipva.png)

### Modules Diagram

![Athena App Modules Diagram](https://res.cloudinary.com/dbc1hdxqi/image/upload/v1596215431/athena-project/assets/AthenaAppProject-Modules_3_ltx8jc.png)
